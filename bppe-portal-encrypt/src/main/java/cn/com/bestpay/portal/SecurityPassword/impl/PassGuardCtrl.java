package cn.com.bestpay.portal.SecurityPassword.impl;

import cn.com.bestpay.portal.SecurityPassword.PasswordInf;
import cn.com.bestpay.portal.common.utils.Charset;
import lombok.extern.slf4j.Slf4j;
import ocx.AESWithJCE;
import ocx.GetRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * 密码卫士控件实现
 * File                 : PassGuardCtrl.java
 *
 */
@Slf4j
public class PassGuardCtrl implements PasswordInf {

	/*
	 * (non-Javadoc)
	 * @see cn.tisson.cmpf.util.pwd.PasswordInf#getRandom(int, java.util.Map)
	 */
	public String getRandom(int len, Map<String, String> params) throws Exception {
		if(0>=len){
			log.error("生成密码随机因子长度为空");
		}
		String mcrypt_key=GetRandom.generateString(len);
		return mcrypt_key;
	}
	/*
	 * (non-Javadoc)
	 * @see cn.tisson.cmpf.util.pwd.PasswordInf#decode(java.lang.String, java.lang.String, java.util.Map)
	 */
	public String decode(String random, String pwdStr, Map<String, String> params) throws Exception {
		String password = null;
		try{
			password = AESWithJCE.getResult(random, pwdStr);
		}catch (Exception e) {
			log.error("密码解密出错",e);
		}
		return password;
	}
	/*
	 * (non-Javadoc)
	 * @see cn.tisson.cmpf.util.pwd.PasswordInf#writePWDInput(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.util.Map)
	 */
	public String writePWDInput(String id, String clazz, String name, String rdName, String sessionKey, Map<String, String> params, HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		String rd = (String) session.getAttribute(sessionKey);
		String target = "PassGuardCtrl"+id.toLowerCase() , showId = "_id_PassGuardCtrl_"+id;
		String str = "<script type=\"text/javascript\">" +
				"var "+target+" = new $.pge({pgeClass:\""+clazz+"\",pgeId: \""+id+"-self\",pgeOnblur:\"config.passOnBlur()\",pgeOnfocus:\"config.passOnFocus()\",pgeWindowID:\"password\"+new Date().getTime()});" +
				"jQuery(function(){"+target+".generate(\""+showId+"\");"+target+".pgInitialize();"+target+".pwdSetSk(\""+rd+"\");});" +
				" window.SECURITY_PASSWORD_"+id+"="+target+"; </script>" +
				"<input type=\"hidden\" name=\""+name+"\" id=\""+id+"\" class=\""+clazz+"\" rd=\""+rd+"\"  objId=\""+id+"-self\"/><div id=\"" + showId + "\"></div>";
		if(!Charset.isEmpty(rdName)) {
			session.setAttribute(rdName, rd);
		}
		return str;
	}
	/*
	 * (non-Javadoc)
	 * @see cn.tisson.cmpf.util.pwd.PasswordInf#encode(java.lang.String, java.util.Map)
	 */
	public String encode(String pwd, Map<String, String> params) throws Exception {
		return pwd;
	}
}