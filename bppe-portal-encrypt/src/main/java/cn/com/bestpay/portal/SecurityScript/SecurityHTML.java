package cn.com.bestpay.portal.SecurityScript;

import cn.com.bestpay.portal.SecurityPassword.impl.Password;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by susie on 2016/10/25.
 */
@Service
public class SecurityHTML  {
    private static Logger logger = LoggerFactory.getLogger(SecurityHTML.class);

    @Autowired
    HttpSession session;

    /**
     * 密码控件 web html
     * @param id 密码控件的ID
     * @param clazz 密码控件及密码隐藏域class,用来设置密码加密后的值,多个密码控件,这个值必须相同
     * @param name 密码隐藏域名字,用来后台取得密码值
     * @param rdName 随机因子数隐藏域名,用来后台取值,如果为空则不生成随机因子隐藏域
     * @param sessionKey session存储随机因子的KEY
     * @param rdFromSession 随机因子数是否从session取,true 则在session取,false则从新生成
     * @return
     */
    public String getSecurityHTML(String id, String clazz, String name, String rdName, String sessionKey, boolean rdFromSession, String params,
            HttpServletRequest request){
        try {
            Password.initPwdImpl("cn.com.bestpay.portal.SecurityPassword.impl.PassGuardCtrl",request);
            return Password.writePWDInput(id, clazz, name, rdName, sessionKey,rdFromSession,request,params);
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage().toString());
        }
        return null;
    }

    public String getSecurityRD(String id, String clazz, String name, String rdName, String sessionKey, boolean rdFromSession, String params,
                                  HttpServletRequest request){
        try {
            getSecurityHTML(id, clazz, name, rdName, sessionKey,rdFromSession,params,request);
            return session.getAttribute(rdName).toString();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error(e.getMessage().toString());
        }
        return null;
    }

}
