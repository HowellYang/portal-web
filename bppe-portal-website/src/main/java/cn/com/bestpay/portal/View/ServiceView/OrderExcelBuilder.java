package cn.com.bestpay.portal.View.ServiceView;


import cn.com.bestpay.portal.model.pojo.ViewModel.OrderModel;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.web.servlet.view.document.AbstractXlsxView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * Created by Howell on 28/11/16.
 */
public class OrderExcelBuilder extends AbstractXlsxView {

    @Override
    protected void buildExcelDocument(Map<String, Object> map, Workbook workbook, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws Exception {

        httpServletResponse.setHeader("Content-Disposition",
                "attachment; filename="+ new String ("订单".getBytes("gb2312"),"iso-8859-1") +".xlsx");

        // get data model which is passed by the Spring container
        List<OrderModel> listOrders = (List<OrderModel>) map.get("listOrders");

        // create a new Excel sheet
        Sheet sheet =  workbook.createSheet("listOrders");
        sheet.setDefaultColumnWidth(30);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // create header row
        Row header = sheet.createRow(0);
        String[] headerNameStr = {"订单号","外部订单号","交易时间","订单类型","业务类型","产品类型",
                "企业账户","支付账户","业务对象","应付金额(元)","实付金额(元)","订单状态"};
        for (int i = 0; i < headerNameStr.length; i++) {
            header.createCell(i).setCellValue(headerNameStr[i]);
            header.getCell(i).setCellStyle(style);
        }

        // create data rows
        int rowCount = 1;

        // set data
        for (OrderModel aOrder : listOrders) {
            Row aRow = sheet.createRow(rowCount++);
            aRow.createCell(0).setCellValue(aOrder.getOrderID());
            aRow.createCell(1).setCellValue(aOrder.getOutsideOrderID());
            aRow.createCell(2).setCellValue(aOrder.getTradingTime());
            aRow.createCell(3).setCellValue(aOrder.getOrderType());
            aRow.createCell(4).setCellValue(aOrder.getServiceType());
            aRow.createCell(5).setCellValue(aOrder.getProductType());
            aRow.createCell(6).setCellValue(aOrder.getEnterpriseAccount());
            aRow.createCell(7).setCellValue(aOrder.getPaymentAccount());
            aRow.createCell(8).setCellValue(aOrder.getServiceObject());
            aRow.createCell(9).setCellValue(aOrder.getSumPayable());
            aRow.createCell(10).setCellValue(aOrder.getPaymentAmount());
            aRow.createCell(11).setCellValue(aOrder.getOrderState());
        }
    }
}
