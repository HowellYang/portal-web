package cn.com.bestpay.portal.config.filter;

import com.alibaba.dubbo.rpc.*;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONArray;
import org.springframework.stereotype.Service;

/**
 * Created by Howell on 10/10/16.
 */
@Service
@Slf4j
public class DubboFilter implements Filter {

    public Result invoke(Invoker<?> invoker, Invocation invocation) throws RpcException {
        String methodName = invocation.getMethodName();
        String className = invoker.getInterface().getSimpleName();
        String serverAddress = invoker.getUrl().getAddress();
        Object[] args = invocation.getArguments();
        if (args.length > 0) {
            log.info("[{}.{}] Start to calling remote [{}] . send request:[{}] ", className, methodName, serverAddress, JSONObject.toJSON(args));
        } else {
            log.info("[{}.{}] Start to calling remote [{}] . ",className, methodName, serverAddress);
        }
        Result result = null;
        Long startTime = System.currentTimeMillis();
        try {
            result = invoker.invoke(invocation);
        } finally {
            Long endTime = System.currentTimeMillis();
            Long elapsed = endTime - startTime;
            if (result != null && result.getValue() != null) {
                log.info("[{}.{}]  Finish calling remote . receive response:[{}].", className, methodName, JSONObject.toJSON(result.getValue()));
            } else {
                log.info("[{}.{}]  Finish calling remote .", className, methodName);
            }
            log.info("[{}.{}]  Elapsed:{} ms.",className, methodName,elapsed);
        }
        String keep = "";
        if (args[0] instanceof com.bestpay.bppe.portal.model.request.base.CommonRequest) {
            //门户service
            keep = ((com.bestpay.bppe.portal.model.request.base.CommonRequest) args[0]).getKeep();
        }
        //打印交易前日志
        doLogBefore(invoker, invocation, keep);

        //处理异常
        handleException(result.getException(), keep);

        //打印交易后日志
        doLogAfter(invoker, result.getValue(), invocation, keep);

        return result;
    }

    /**
     * 处理异常
     *
     * @param th 异常
     */
    private void handleException(Throwable th, String keep) {
        if (th == null) {
            return;
        }
    }
    private void doLogBefore(Invoker<?> invoker, Invocation invocation, String keep) {
        log.info(BEFORE_LOG_MSG,
                keep, invoker.getInterface().getSimpleName(), invocation.getMethodName(),
                invoker.getUrl().getAddress(), getLog(keep,invocation.getArguments()));
    }

    private void doLogAfter(Invoker<?> invoker, Object response, Invocation invocation, String keep) {
        String clazzName = invoker.getInterface().getSimpleName();   //获得类名
        String methodName = invocation.getMethodName();
        if (response == null) {
            log.warn(AFTER_LOG_MSG_NULL, keep, clazzName, methodName, "null");
            return;
        }
        log.info(AFTER_LOG_MSG, keep, clazzName, methodName, getLog(keep, response));
    }

    private Object getLog(String keep,Object args){
        try {
            if(args != null){
                args = JSONArray.fromObject(args);
            }
        }catch (Exception e){
            log.error("[{}] 参数打印错误,类:[{}],异常:[{}]", keep, this.getClass().getName(), e);
        }
        return args;
    }

    private static final String BEFORE_LOG_MSG = "[{}] Calling remote service[{}->{}][{}].Send request:{}.";
    private static final String AFTER_LOG_MSG = "[{}] Called remote service[{}->{}].response:[{}]";
    private static final String AFTER_LOG_MSG_NULL = "[{}] Called remote service[{}->{}].But response is [{}]";
}
