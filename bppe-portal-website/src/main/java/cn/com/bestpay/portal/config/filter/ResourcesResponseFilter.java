package cn.com.bestpay.portal.config.filter;

import cn.com.bestpay.portal.config.filter.tool.CharResponseWrapper;
import cn.com.bestpay.portal.config.filter.tool.SetVersion;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by yfzx_gd_yanghh on 2016/9/28.
 */
@WebFilter(
        filterName = "ResourcesResponseFilter",
        urlPatterns = {"*.html","*.js","*.css","*.hbs"}
)
@Slf4j
public class ResourcesResponseFilter implements Filter {

    public ResourcesResponseFilter() {
    }

    public void init(FilterConfig filterConfig) throws ServletException {

    }

    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
         // 请求的uri
        try {
            String url = ((HttpServletRequest)servletRequest).getRequestURI();

            CharResponseWrapper crw = new CharResponseWrapper((HttpServletResponse)servletResponse);
            filterChain.doFilter(servletRequest, crw);

            String content = crw.getContent();//response流的内容

            //此处可以对content做处理,然后再把content写回到输出流中
            String extensionName = getExtensionName(url);

            if(extensionName.equals("js")){
//                if( url.equals("/lib/js/bestpay/bestpay.global.js")
//                        && SystemProperty.getValueParam("system.debug").equals("false")){
//                    String miniJS = CompilerJs.miniJS(content);
//                    if (!miniJS.equals("JS Closure Errors!")){
//                        content = miniJS;
//                    }
//                }
                content = content.replaceAll("&#x27;","\'").replaceAll("&quot;","\"").replaceAll("&amp;","&");
                content = SetVersion.chinaToUnicode(content);
            }

            if(extensionName.equals("html")){
                content = content.replaceAll("&#x27;","\'").replaceAll("&quot;","\"").replaceAll("&amp;","&").
                        replaceAll("&lt;","<").replaceAll("&#x3D;","=").replaceAll("&gt;",">");
            }

            if(extensionName.equals("html") || extensionName.equals("hbs")){
                servletResponse.setContentLength(-1);
                ((HttpServletResponse) servletResponse).addHeader("Content-Type","text/html;charset=UTF-8");
            } else {
                servletResponse.setContentLength(content.getBytes().length);
            }

            ((HttpServletResponse) servletResponse).addHeader("Access-Control-Allow-Origin","*");
            PrintWriter out = servletResponse.getWriter();
            out.write(content);
            out.flush();
            out.close();

        } catch (Exception e){
            PrintWriter out = servletResponse.getWriter();
            out.write("");
            out.flush();
            out.close();
            log.error(e.getMessage().toString());
        }
    }

    public void destroy() {

    }
    public static String getExtensionName(String filename) {
        if ((filename != null) && (filename.length() > 0)) {
            int dot = filename.lastIndexOf('.');
            if ((dot >-1) && (dot < (filename.length() - 1))) {
                return filename.substring(dot + 1);
            }
        }
        return filename;
    }
}
