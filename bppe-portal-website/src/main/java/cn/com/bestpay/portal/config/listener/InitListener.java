package cn.com.bestpay.portal.config.listener;

import cn.com.bestpay.portal.config.property.SystemProperty;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;


/**
 * Created by yfzx_gd_yanghh on 2016/9/29.
 */
@WebListener
@Slf4j
public class InitListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        log.info("ServletContex初始化");
        SystemProperty systemProperty = new SystemProperty();
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        log.info("ServletContex销毁");
    }


}
