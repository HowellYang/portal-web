package cn.com.bestpay.portal.config.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

/**
 * Created by Howell on 8/2/17.
 */
@Component
public class JedisExecService {

//    @Autowired
//    JedisConnectionFactory jedisConnectionFactory;
//
//    @Bean
//    RedisTemplate< String, Object > redisTemplate() {
//        final RedisTemplate< String, Object > template =  new RedisTemplate< String, Object >();
//        template.setConnectionFactory(jedisConnectionFactory);
//        template.setKeySerializer( new StringRedisSerializer() );
//        template.setHashValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
//        template.setValueSerializer( new GenericToStringSerializer< Object >( Object.class ) );
//        return template;
//    }

    @Autowired
    RedisTemplate<String, Object> redisTemplate;

    public void set(final String key, final String value) {
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        opsForValue.set(key, value);
    }

    public Object get(final String key) {
        ValueOperations<String, Object> opsForValue = redisTemplate.opsForValue();
        return opsForValue.get(key);
    }

    public void delete(final String key) {
        redisTemplate.delete(key);
    }


}
