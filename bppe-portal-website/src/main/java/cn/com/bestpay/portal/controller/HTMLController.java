package cn.com.bestpay.portal.controller;


import cn.com.bestpay.portal.config.property.SystemProperty;
import com.bestpay.bppe.portal.model.response.base.CommonResponse;
import com.bestpay.bppe.portal.model.response.password.query.VerifyLoginPasswordResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by yfzx_gd_yanghh on 2016/9/28.
 */
@Controller
@Slf4j
public class HTMLController extends BaseController{

    @Autowired
    HttpSession session;

    @RequestMapping(value = "/{First}.html", method = RequestMethod.GET)
    public ModelAndView First(@PathVariable("First")String First) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("userName", "Hi!");
        return new ModelAndView(First, map);
    }

    @RequestMapping(value = "/{First}/{Second}.html", method = RequestMethod.GET)
    public ModelAndView Second(@PathVariable("First")String First,
                               @PathVariable("Second")String Second,
                               HttpServletRequest request
                               ) {
        Map<String, Object> map = new HashMap<String, Object>();

        map.put("userName", "'Hi',首页!");
//        List<AppcenterModel> modelList = new ArrayList<>();
//        AppcenterModel appcenterModel = new AppcenterModel();
//        appcenterModel.setAppId("1");
//        appcenterModel.setAppName("yang");
//        appcenterModel.setAppUrl("123123");
//        appcenterModel.setIsTrue("true");
//        modelList.add(appcenterModel);
//        appcenterModel = new AppcenterModel();
//        appcenterModel.setAppId("2");
//        appcenterModel.setAppName("tang");
//        appcenterModel.setAppUrl("123123");
//        appcenterModel.setIsTrue("false");
//        modelList.add(appcenterModel);
//        appcenterModel = new AppcenterModel();
//        appcenterModel.setAppId("3");
//        appcenterModel.setAppName("tang");
//        appcenterModel.setAppUrl("123123");
//        appcenterModel.setIsTrue("true");
//        modelList.add(appcenterModel);
//        map.put("_DATA_", modelList);

        String CDN_Url = SystemProperty.getValueParam("system.CDN_Url");
        StringBuffer stringBuffer = new StringBuffer();
        if(SystemProperty.getValueParam("system.debug").equals("true")){
//            stringBuffer.append("<link type=\"text/css\" rel=\"stylesheet\" href=\""+CDN_Url+"/lib/css/jasmine.css\" media=\"screen\" />");
//            stringBuffer.append("<script src=\""+CDN_Url+"/lib/js/testFrame/jasmine.js\"></script>");
//            stringBuffer.append("<script src=\""+CDN_Url+"/lib/js/testFrame/jasmine-html.js\"></script>");
//            stringBuffer.append("<script src=\""+CDN_Url+"/lib/js/testFrame/boot.js\"></script>");
        } else {
            stringBuffer.append("");
        }
        map.put("testFrame", stringBuffer) ;

        if(session.getAttribute("userSession") != null) {
            map.put("isLogin", "true");
        } else {
            map.put("isLogin", "false");
        }

        if(session.getAttribute("LoginErrorMsg") != null){
            CommonResponse<VerifyLoginPasswordResponse> respVer =( CommonResponse<VerifyLoginPasswordResponse> ) session.getAttribute("LoginErrorMsg");
            map.put("LoginErrorMsg", respVer.getErrorMsg());
            session.setAttribute("LoginErrorMsg", null);
        }

        return new ModelAndView(First+"/"+Second, map);
    }

}
