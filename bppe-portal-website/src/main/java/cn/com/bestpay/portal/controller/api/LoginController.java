package cn.com.bestpay.portal.controller.api;

import cn.com.bestpay.portal.common.utils.Charset;
import cn.com.bestpay.portal.controller.BaseController;
import cn.com.bestpay.portal.model.pojo.UtilsModel.VCodeModel;
import cn.com.bestpay.portal.service.LoginService;
import cn.com.bestpay.portal.service.api.vcode.VCodeBuilderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by yfzx_gd_yanghh on 2016/10/15.
 */
@Controller
@Slf4j
public class LoginController extends BaseController {

    @Autowired
    public LoginService loginService;

    @Autowired
    public VCodeBuilderService vCodeBuilderService;

    /**
     * 门户登录
     * @param request
     * @param session
     * @return
     */
    @RequestMapping(value = "/api/login", method = RequestMethod.POST)
    public ModelAndView userLogin(HttpServletRequest request, HttpSession session) {

        Future<Boolean> future = loginService.userLogin(request);



        try {
            if(future.get()){
                return new ModelAndView("redirect:/Index/main.html#/Index");
            } else {
                return new ModelAndView("redirect:/Index/main.html#/Login");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return new ModelAndView("redirect:/Index/main.html#/Login");
    }

    /**
     * 登录验证码获取
     * @param request
     * @param response
     * @param session
     * @return
     * @throws ServletException
     * @throws IOException
     */
    @RequestMapping(value = "/api/vcode", method = RequestMethod.GET)
    public void userVCode(HttpServletRequest request, HttpServletResponse response,
                                        HttpSession session) {
        String referer = request.getHeader("Referer"), vCodeHeight, vCodeWidth;
        VCodeModel vCode = null;
        // 过滤非法途径进入;
        if (referer == null || !referer.matches("^https?://" + request.getServerName() + ".*")) {
            // 非法途径,返回首页
            try {
                response.sendRedirect("/Index/main.html");
            } catch (IOException e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
            return;
        }
        response.setHeader("Pragma", "no-cache");
        response.setHeader("Cache-Control", "no-cache");
        response.setDateHeader("Expires", 0);
        response.setContentType("image/jpeg");

        vCodeHeight = request.getParameter("vCodeHeight");
        vCodeWidth = request.getParameter("vCodeWidth");

        if (!Charset.isEmpty(vCodeWidth) && !Charset.isEmpty(vCodeHeight)) {
            vCode = vCodeBuilderService.generate(Integer.parseInt(vCodeWidth), Integer.parseInt(vCodeHeight));
        } else {
            vCode = vCodeBuilderService.generate();
        }
        session.setAttribute("LOGIN_VERIFY_CODE", vCode.getValue());
        try {
            OutputStream out = response.getOutputStream();
            ImageIO.write((RenderedImage) vCode.getImage(), "jpeg", out);
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }

    }
}
