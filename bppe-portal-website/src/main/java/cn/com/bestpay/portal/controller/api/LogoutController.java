package cn.com.bestpay.portal.controller.api;

import cn.com.bestpay.portal.controller.BaseController;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

/**
 * Created by yfzx_gd_yanghh on 2016/10/17.
 */
@Controller
@Slf4j
public class LogoutController extends BaseController {

    @RequestMapping(value = "/api/logout", method = RequestMethod.GET)
    public ModelAndView userLogout( HttpSession session) {
        session.removeAttribute("userSession");
        session.invalidate();
        session = null;

        return new ModelAndView("redirect:/Index/main.html");
    }
}
