package cn.com.bestpay.portal.controller.api.index;

import cn.com.bestpay.portal.config.redis.JedisExecService;
import cn.com.bestpay.portal.controller.BaseController;
import cn.com.bestpay.portal.model.pojo.UtilsModel.UserInfoModel;
import cn.com.bestpay.portal.model.resp.ParentResp;
import cn.com.bestpay.portal.service.api.index.IndexService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * Created by Howell on 6/10/16.
 */
@RestController
@Slf4j
public class IndexController extends BaseController {

    @Autowired
    HttpSession session;

    @Autowired
    IndexService indexService;

    @Autowired
    JedisExecService jedisExecService;

    @RequestMapping(value = "/api/index/main",method = RequestMethod.POST)
    @ResponseBody
    public ParentResp RestMain(@RequestBody String body, HttpServletRequest request){
        log.info("RequestBody:[{}]", body);

        UserInfoModel userInfoModel =  (UserInfoModel) session.getAttribute("userSession");
        userInfoModel.setCustName("成功");
        session.setAttribute("userSession",userInfoModel);

        jedisExecService.set("abc","abc");

        log.info("abc:[{}]", jedisExecService.get("abc").toString());

        ParentResp parentResp = null;
        try {
            Future<ParentResp> future = indexService.indexMain();
            parentResp = future.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return parentResp;

    }
}
