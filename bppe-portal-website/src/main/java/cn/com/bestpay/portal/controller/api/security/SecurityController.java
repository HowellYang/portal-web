package cn.com.bestpay.portal.controller.api.security;

import cn.com.bestpay.portal.SecurityScript.SecurityHTML;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Howell on 16/11/16.
 */
@Controller
@Slf4j
public class SecurityController {

    @Autowired
    SecurityHTML securityHTML;

    /**
     * 登陆安全密码控件
     * @return
     */
    @RequestMapping(value = "/WebCommon/Security/LoginPasswordView.hbs", method = RequestMethod.GET)
    @ResponseBody
    public String getLoginPasswordView(HttpServletRequest request) {
        String HTML = securityHTML.getSecurityHTML("loginpwd", "loginpwd", "loginpwd",
                "login-rd", "login-pwd-ses-key", false, "pwdmark:0", request);
        return HTML;
    }

    /**
     * 支付安全密码控件
     * @return
     */
    @RequestMapping(value = "/WebCommon/Security/PayPasswordView.hbs", method = RequestMethod.GET)
    @ResponseBody
    public String getPayPasswordView(HttpServletRequest request) {
        String HTML = securityHTML.getSecurityHTML("paypwd", "paypwd", "paypwd",
                "pay-rd", "pay-pwd-ses-key", false, "pwdmark:0", request);
        return HTML;
    }


}
