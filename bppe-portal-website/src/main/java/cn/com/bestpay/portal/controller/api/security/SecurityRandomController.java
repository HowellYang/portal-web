package cn.com.bestpay.portal.controller.api.security;

import cn.com.bestpay.portal.SecurityScript.SecurityHTML;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by Howell on 27/10/16.
 */
@Controller
public class SecurityRandomController {

    @Autowired
    HttpSession session;

    @Autowired
    SecurityHTML securityHTML;

    @RequestMapping(value = "/api/security/random", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> speedGetRandom(HttpServletRequest request) {
        JSONObject jsonObject = new JSONObject();
        try {
            String RD = securityHTML.getSecurityRD("index", "", "SecurityHTML_Index", "SecurityHTML_Index_rd",
                    "SecurityHTML_Index_Key", false, "pwdmark:0", request );
            jsonObject.put("SecurityScriptRD", RD);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return JSONObject.fromObject(jsonObject.toString());
    }

    @RequestMapping(value = "/api/security/test", method = RequestMethod.POST)
    @ResponseBody
    public Map<String,Object> speedGetTest(HttpServletRequest request) {
        JSONObject jsonObject = new JSONObject();

        jsonObject.put("code","000000");
        jsonObject.put("content","成功123");

        return JSONObject.fromObject(jsonObject.toString());
    }
}
