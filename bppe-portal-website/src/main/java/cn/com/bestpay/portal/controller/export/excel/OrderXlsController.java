package cn.com.bestpay.portal.controller.export.excel;


import cn.com.bestpay.portal.model.pojo.ViewModel.OrderModel;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.googlecode.htmlcompressor.compressor.Compressor;
import com.googlecode.htmlcompressor.compressor.XmlCompressor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.XmlViewResolver;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by Howell on 28/11/16.
 * xls 导出 demo
 */
@Controller
@Slf4j
public class OrderXlsController extends HttpServlet {
    @Autowired
    XmlViewResolver viewResolver;

    /**
     * 数据量少xls下载,处理方式
     * @param request
     * @param response
     * @param session
     * @return
     */
    @RequestMapping(value = "/api/export/OrderXls", method = {RequestMethod.POST})
    public ModelAndView RestOrderXls(HttpServletRequest request, HttpServletResponse response,
                                     HttpSession session){
        List<OrderModel> listOrders = new ArrayList<OrderModel>();

        //TODO:添加获取到的数据
        listOrders.add(new OrderModel("1","2","3","4","5",
        "6","7","8","9","10",
        "11","12"));

        listOrders.add(new OrderModel("1","2","3","4","5",
                "6","7","8","9","10",
                "11","12"));

        View view = null;
        try {
            view = viewResolver.resolveViewName("OrderExcelView", Locale.CHINA);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //推送到view层处理数据跟xls文件格式
        return new ModelAndView(view, "listOrders",listOrders);
    }


    /**
     * 大数据量xls下载,处理方式
     * @param request
     * @param response
     * @param session
     */
    @RequestMapping(value = "/api/export/OrderBigXls", method = {RequestMethod.POST})
    public void RestBigOrderXls(HttpServletRequest request, HttpServletResponse response,
                                HttpSession session){
        PrintWriter out = null;
        Handlebars handlebars = new Handlebars();
        Compressor compressor = new XmlCompressor();
        try {
            out = response.getWriter();
            response.reset();
            String fileName = request.getParameter("fileName");
            log.debug("OrderBigXls fileName: "+fileName);

            response.setHeader("Content-Disposition", "attachment; filename="+  fileName +".xls");
            response.setContentType("application/octet-stream; charset=utf-8");

            //xls 文件头模板加载
            Template template =  handlebars.compile("templates/order/header");
            Map map = new HashMap<String, Object>();
            map.put("sheetName", "abc");  //sheet 名称
            map.put("columnNum", "12");   //配置例数，必须等于、大于数据例数
            map.put("rowNum", 9000000);   //配置行数，必须等于、大于数据行数
            String result = template.apply(map);

            //输出给文件头浏览器下载
            out.write(compressor.compress(result));

            //xls 数据模板加载
            template =  handlebars.compile("templates/order/body");

            //todo: 分批获取数据，输出给浏览器下载
            for(int i=0;i<2000;i++){
                List<OrderModel> modelList = new ArrayList<>();

                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));
                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));
                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));
                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));
                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));
                modelList.add(new OrderModel(i+"1","2","3","4","5",
                        "6","7","8","9","10",
                        "11","12"));

                map.put("dataList", modelList);
                result = template.apply(map);
                out.write(compressor.compress(result));
            }

            //xls 文件尾模板加载
            template =  handlebars.compile("templates/order/footer");
            out.write(compressor.compress(template.text()));

            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }  finally {
            if (out != null) {
                out.close();
            }
        }


    }


}
