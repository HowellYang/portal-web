/**
 * @license Highcharts JS v2.3.3 (2012-11-02)
 *
 * (c) 20012-2014
 *
 * Author: Gert Vaartjes
 *
 * License: www.highcharts.com/license
 */
package cn.com.bestpay.portal.controller.export.highcharts;

import lombok.extern.slf4j.Slf4j;
import org.apache.batik.transcoder.Transcoder;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.JPEGTranscoder;
import org.apache.batik.transcoder.image.PNGTranscoder;
import org.apache.fop.svg.PDFTranscoder;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.StringReader;

@Controller
@Slf4j
public class ExportController extends HttpServlet {

	@RequestMapping(value = "/api/export/highcharts", method = {RequestMethod.POST})
	public HttpEntity<byte[]> RestExporter(
		@RequestParam(value = "svg", required = false) String svg,
		@RequestParam(value = "type", required = false) String type,
		@RequestParam(value = "filename", required = false) String filename,
		@RequestParam(value = "width", required = false) String width,
		@RequestParam(value = "scale", required = false) String scale,
		@RequestParam(value = "options", required = false) String options,
		@RequestParam(value = "globaloptions", required = false) String globalOptions,
		@RequestParam(value = "constr", required = false) String constructor,
		@RequestParam(value = "callback", required = false) String callback,
		@RequestParam(value = "resources", required = false) String resources,
		@RequestParam(value = "async", required = false, defaultValue = "false")  Boolean async,
		HttpServletRequest request, HttpServletResponse response,
		HttpSession session) throws IOException {

		type = request.getParameter("type");
		svg = request.getParameter("svg");

		request.setCharacterEncoding("utf-8");//设置编码，解决乱码问题
		filename = filename==null?"chart":filename;
		ServletOutputStream out = response.getOutputStream();
		if (null != type && null != svg) {
			svg = svg.replaceAll(":rect", "rect");
			String ext = "";
			Transcoder t = null;
			if (type.equals("image/png")) {
				ext = "png";
				t = new PNGTranscoder();
			}
			else if (type.equals("image/jpeg")) {
				ext = "jpg";
				t = new JPEGTranscoder();
			}
			else if(type.equals("image/svg+xml")){
				ext = "svg";
			}
			else if (type.equals("application/pdf")) {
				ext = "pdf";
				t = new PDFTranscoder();
			}
			response.addHeader("Content-Disposition", "attachment; filename="+ new String (filename.getBytes("gb2312"),"iso-8859-1") + "."+ext);
			response.addHeader("Content-Type", type);

			if (null != t) {
				TranscoderInput input = new TranscoderInput(new StringReader(svg));
				TranscoderOutput output = new TranscoderOutput(out);

				try {
					if (type.equals("image/png")) {
						PNGTranscoder pngTranscoder = new PNGTranscoder();
						pngTranscoder.transcode(input, output);
					} else if (type.equals("image/jpeg")) {
						JPEGTranscoder jpegTranscoder = new JPEGTranscoder();
						jpegTranscoder.addTranscodingHint(JPEGTranscoder.KEY_QUALITY, 0.99f);
						jpegTranscoder.transcode(input, output);

					} else {
						t.transcode(input, output);
					}

				} catch (TranscoderException e) {
					out.print("Problem transcoding stream. See the web logs for more details.");
					e.printStackTrace();
				}
			} else if (ext.equals("svg")) {
				OutputStreamWriter writer = new OutputStreamWriter(out, "UTF-8");
				writer.append(svg);
				writer.close();
			} else
				out.print("Invalid type: " + type);
		} else {
			response.addHeader("Content-Type", "text/html");
			out.println("Usage:\n\tParameter [svg]: The DOM Element to be converted." +
					"\n\tParameter [type]: The destination MIME type for the elment to be transcoded.");
		}
		out.flush();
		out.close();

		return null;
	}



}
