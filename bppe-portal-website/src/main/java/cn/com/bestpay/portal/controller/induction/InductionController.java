package cn.com.bestpay.portal.controller.induction;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Howell on 25/11/16.
 */
@Controller
@Slf4j
public class InductionController extends HttpServlet {

    @RequestMapping(value = "/api/induction/dataFile", method = {RequestMethod.POST})
    public HttpEntity<byte[]> RestInductionFile(@RequestParam("file") MultipartFile file,
                                           HttpServletRequest request, HttpServletResponse response,
                                           HttpSession session){

        if (!file.isEmpty()) {
            try {
                byte[] bytes = file.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }
            // store the bytes somewhere
            //在这里就可以对file进行处理了，可以根据自己的需求把它存到数据库或者服务器的某个文件夹
            log.info("redirect:uploadSuccess");

            try {
                ServletOutputStream out = response.getOutputStream();
                response.addHeader("Content-Type", "text/plain; charset=UTF-8");
                out.print("{\"code\":\"000000\",\"content\":\"UploadSuccess\"}");

                out.flush();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            log.info("redirect:uploadFailure");
        }

        return null;
    }
}
