package cn.com.bestpay.portal.model.pojo.UtilsModel;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by yfzx_gd_yanghh on 2016/10/15.
 */
@Data
@ToString(callSuper = true)
public class UserInfoModel implements Serializable {
    /**
     * @Description 登录用户名
     */
    private String staffCode;
    /**
     * @Description 令牌
     */
    private String tokenCode;
    /**
     * @Description 客户编码
     */
    private String custCode;
    /**
     * @Description 接入机构商户编码
     */
    private String prtnCode;
    /**
     * 合作伙伴类型
     * <li>PT101平台运营商</li>
     * <li>PT201支付提供商</li>
     * <li>PT301服务提供商</li>
     * <li>PT401渠道商户</li>
     * <li>PT403接入机构</li>
     * <li>PT901代理</li>
     * <li>PT404平台租赁商</li>
     */
    private String partnerPrtnType;
    /**
     * @Description 登录客户权限列表
     */
    private String privUri;
    /**
     * @Description 账户标识（0:试用账户，1：正式账户）
     */
    private String acctStat;
    /**
     * @Description 注册类型（0:个人商户，1：企业商户）
     */
    private String regType;
    /**
     * @Description
     *              产品类型（100：企业账户，101：代收付，102：IPOS，103：批扣，104：手机IPOS，200：EPOS，201
     *              ：ATM，202：多媒体，203：自助终端）,其中手机端产品类型为三种：100、104、100|104
     */
    private String products;
    /**
     * @Description 产品线类型（0:不支持、1：资金归集、2：手机交费易、3：双产品线）
     */
    private int productType;
    /**
     * @Description 资金管理模式（BT1001：普通卡，BT1002：子母卡，BT1013：资金池母卡，BT1014：资金池子卡）
     */
    private String bankMode;

    private String email;
    /**
     * @Description 注册渠道（20：手机客户端，90：门户运营）
     */
    private String regChanal;
    /**
     * @Description 是否绑卡（0：未绑卡，1：已绑卡，2：绑卡中）
     */
    private String bindCard;
    /**
     * @Description 审核状态（S0V:待审核,S0A:审核通过,S0F:审核不通过）
     */
    private String aproStat;
    /**
     * @Description 审核意见
     */
    private String aproDesc = "";

    /**
     * PRINTYPE
     * 代理商：  PT901    普通商户：PT403
     */
    private String prinType;

    /**
     * 认证状态: A00 认证中，A01未认证，A02已认证，A99认证失败
     */
    private String authenStatus;

    /**
     * 商户名称
     */
    private String custName;

    /**
     * 地区编码
     */
    private String areaCode;

    /**
     * 添益宝标识
     */
    private String hadEpt;

    /**
     * mac 地址
     */
    private String MachineNetwork;

    /**
     *  磁盘编号
     */
    private String MachineDisk;

    /**
     * cpu编号
     */
    private String MachineCPU;


}
