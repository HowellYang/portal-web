package cn.com.bestpay.portal.model.pojo.UtilsModel;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * Created by Howell on 16/1/17.
 */
@Data
@ToString(callSuper = true)
public class ViewModel implements Serializable {
    /**
     * 商户名称
     */
    private String custName;
    /**
     * @Description 令牌
     */
    private String tokenCode;
}
