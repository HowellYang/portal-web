package cn.com.bestpay.portal.model.pojo.ViewModel;

/**
 * Created by Howell on 28/11/16.
 */
public class OrderModel {
    private String orderID;
    private String outsideOrderID;
    private String tradingTime;
    private String orderType;
    private String serviceType;
    private String productType;
    private String enterpriseAccount;
    private String paymentAccount;
    private String serviceObject;
    private String sumPayable;
    private String paymentAmount;
    private String orderState;

    public OrderModel(String orderID, String outsideOrderID, String tradingTime, String orderType, String serviceType, String productType, String enterpriseAccount, String paymentAccount, String serviceObject, String sumPayable, String paymentAmount, String orderState) {
        this.orderID = orderID;
        this.outsideOrderID = outsideOrderID;
        this.tradingTime = tradingTime;
        this.orderType = orderType;
        this.serviceType = serviceType;
        this.productType = productType;
        this.enterpriseAccount = enterpriseAccount;
        this.paymentAccount = paymentAccount;
        this.serviceObject = serviceObject;
        this.sumPayable = sumPayable;
        this.paymentAmount = paymentAmount;
        this.orderState = orderState;
    }

    public String getOrderID() {
        return orderID;
    }

    public void setOrderID(String orderID) {
        this.orderID = orderID;
    }

    public String getOutsideOrderID() {
        return outsideOrderID;
    }

    public void setOutsideOrderID(String outsideOrderID) {
        this.outsideOrderID = outsideOrderID;
    }

    public String getTradingTime() {
        return tradingTime;
    }

    public void setTradingTime(String tradingTime) {
        this.tradingTime = tradingTime;
    }

    public String getOrderType() {
        return orderType;
    }

    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    public String getServiceType() {
        return serviceType;
    }

    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getEnterpriseAccount() {
        return enterpriseAccount;
    }

    public void setEnterpriseAccount(String enterpriseAccount) {
        this.enterpriseAccount = enterpriseAccount;
    }

    public String getPaymentAccount() {
        return paymentAccount;
    }

    public void setPaymentAccount(String paymentAccount) {
        this.paymentAccount = paymentAccount;
    }

    public String getServiceObject() {
        return serviceObject;
    }

    public void setServiceObject(String serviceObject) {
        this.serviceObject = serviceObject;
    }

    public String getSumPayable() {
        return sumPayable;
    }

    public void setSumPayable(String sumPayable) {
        this.sumPayable = sumPayable;
    }

    public String getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(String paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getOrderState() {
        return orderState;
    }

    public void setOrderState(String orderState) {
        this.orderState = orderState;
    }
}
