package cn.com.bestpay.portal.service;


import cn.com.bestpay.portal.SecurityPassword.impl.Password;
import cn.com.bestpay.portal.common.utils.AesCipher;
import cn.com.bestpay.portal.common.utils.Charset;
import cn.com.bestpay.portal.common.utils.GetString;
import cn.com.bestpay.portal.model.pojo.UtilsModel.UserInfoModel;
import cn.com.bestpay.portal.model.pojo.UtilsModel.ViewModel;
import com.bestpay.bppe.portal.model.request.password.query.ApplyRandomCodeRequest;
import com.bestpay.bppe.portal.model.request.password.query.VerifyLoginPasswordRequest;
import com.bestpay.bppe.portal.model.response.base.CommonResponse;
import com.bestpay.bppe.portal.model.response.password.query.ApplyRandomCodeResponse;
import com.bestpay.bppe.portal.model.response.password.query.VerifyLoginPasswordResponse;
import com.bestpay.bppe.portal.model.response.password.query.model.DetailCumInfo;
import com.bestpay.bppe.portal.service.password.query.ApplyRandomCodeService;
import com.bestpay.bppe.portal.service.password.query.PasswordVerifyService;
import com.bestpay.common.enums.AppName;
import com.bestpay.common.utils.SerialNoUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.security.GeneralSecurityException;
import java.util.UUID;
import java.util.concurrent.Future;

/**
 * Created by Howell on 17/10/16.
 */
@Service
@Slf4j
public class LoginService {
    @Autowired
    private PasswordVerifyService passwordVerifyService;

    @Autowired
    private ApplyRandomCodeService applyRandomCodeService;

    @Async
    public Future<Boolean> userLogin(HttpServletRequest request){
        //获取web页面传来的值
        String staffCode = request.getParameter("username");
        String password = request.getParameter("loginpwd");
        String vcode = request.getParameter("vcode");
        try {
            //参数解密
            password = Password.decode("login-pwd-ses-key", password, Password.SESSION_SCOPE, request, null);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage().toString());
        }
        //log.debug("password:"+password); // 密码明文

        //校验登陆验证码
        if(!verifyLoginVCpde(request.getSession(), vcode)){
            return new AsyncResult<Boolean>(false);
        }

        //校验登陆密码
        if(verifyLoginPassword(staffCode, password, request, request.getSession())){
            return new AsyncResult<Boolean>(true);
        }
        return new AsyncResult<Boolean>(false);
    }

    /**
     * 校验登陆验证码
     * @param session
     * @param vcode 页面传过来的验证码
     * @return
     */
    public boolean verifyLoginVCpde(HttpSession session, String vcode){
        CommonResponse<VerifyLoginPasswordResponse> respVer = new CommonResponse<VerifyLoginPasswordResponse>();
        Object vCode = session.getAttribute("LOGIN_VERIFY_CODE");
        vCode = null == vCode ? "".intern() : vCode;
        if (Charset.isEmpty(vCode.toString())) {
            respVer.setErrorCode("");
            respVer.setErrorMsg("验证码获取失败");
            session.setAttribute("LoginErrorMsg",respVer);
            return false;
        }
        if (Charset.isEmpty(vcode) || !vCode.toString().toLowerCase().equals(
                vcode.toLowerCase())) {
            respVer.setErrorCode("");
            respVer.setErrorMsg("验证码输入错误");
            session.setAttribute("LoginErrorMsg",respVer);
            return false;
        }
        return true;
    }

    /**
     * 获取加密随机数
     * @return
     */
    public CommonResponse<ApplyRandomCodeResponse> getRandomCode(String key){
        ApplyRandomCodeRequest randomReq = new ApplyRandomCodeRequest();
        randomReq.setKeep(SerialNoUtil.generateSerialNo(AppName.BPP));
        randomReq.setRandomSecKey(key);
        return applyRandomCodeService.random(randomReq);
    }

    /**
     * 校验登陆密码请求参数
     * @param loginPassword
     * @param decryptRandomCodeIndex
     * @param staffCode
     * @param request
     * @return
     */
    public VerifyLoginPasswordRequest verifyLoginPasswordParams(String loginPassword, String decryptRandomCodeIndex,
                                                                String staffCode, HttpServletRequest request){
        VerifyLoginPasswordRequest verifyReq = new VerifyLoginPasswordRequest();
        verifyReq.setKeep(SerialNoUtil.generateSerialNo(AppName.BPP));
        verifyReq.setOperOrgFlag("0.0.0.0");
        verifyReq.setPassword(loginPassword);
        verifyReq.setChannelType("90");
        verifyReq.setRandomCodeIndex(decryptRandomCodeIndex);
        verifyReq.setLoginName(staffCode);
        verifyReq.setRequestIp(GetString.getIpAddr(request));
        verifyReq.setOperSource("127.0.0.1");
        return verifyReq;
    }

    /**
     * 校验登陆密码
     * @param staffCode
     * @param password
     * @param request
     * @param session
     * @return
     */
    public boolean verifyLoginPassword(String staffCode, String password, HttpServletRequest request , HttpSession session){
        //生成随机数
        String key = UUID.randomUUID().toString().replaceAll("-", "");
        //获取密码加密随机数
        CommonResponse<ApplyRandomCodeResponse> respRandom= getRandomCode(key);

        String loginPassword = null, decryptRandNum = null, decryptRandomCodeIndex = null;
        try {
            decryptRandNum = AesCipher.decrypt(key, respRandom.getResult().getRandomCode());                     //随机数明文
            decryptRandomCodeIndex = AesCipher.decrypt(key, respRandom.getResult().getRandomCodeIndex());     //加密因子索引明文
            loginPassword = AesCipher.encrypt(decryptRandNum, password);
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
            log.error(e.getMessage().toString());
        }

        //密码校验
        CommonResponse<VerifyLoginPasswordResponse> respVer = passwordVerifyService.verifyLogin(
                verifyLoginPasswordParams(loginPassword, decryptRandomCodeIndex, staffCode, request));

        String machineNetwork = request.getParameter("machineNetwork");
        String machineCPU = request.getParameter("machineCPU");
        String machineDisk = request.getParameter("machineDisk");
        try {
            machineNetwork = Password.decode("login-pwd-ses-key", machineNetwork, Password.SESSION_SCOPE, request, null);
            machineCPU = Password.decode("login-pwd-ses-key", machineCPU, Password.SESSION_SCOPE, request, null);
            machineDisk = Password.decode("login-pwd-ses-key", machineDisk, Password.SESSION_SCOPE, request, null);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage().toString());
        }

        if("PCA31000000".equals(respVer.getErrorCode())) {
            //校验登陆密码请求成功操作
            DetailCumInfo respResp = respVer.getResult().getDetailCumInfo();
            UserInfoModel userInfoModel = new UserInfoModel();
            userInfoModel.setStaffCode(staffCode);
            userInfoModel.setCustCode(respResp.getCustCode());
            userInfoModel.setAreaCode(respResp.getAreaCode());
            userInfoModel.setCustName(respResp.getCustName());
            userInfoModel.setPartnerPrtnType(respResp.getPartnerPrtnType());
            userInfoModel.setPrinType(respResp.getRegType());
            userInfoModel.setEmail(respResp.getEmail());
            userInfoModel.setBankMode(respResp.getCumAttr2842());

            userInfoModel.setMachineNetwork(machineNetwork);
            userInfoModel.setMachineCPU(machineCPU);
            userInfoModel.setMachineDisk(machineDisk);

            session.setAttribute("userSession",userInfoModel);

            ViewModel viewModel = new ViewModel();
            viewModel.setCustName(respResp.getCustName());
            session.setAttribute("ViewModel", viewModel);
            return true;
        }
        session.setAttribute("LoginErrorMsg",respVer);
        return false;
    }




}
