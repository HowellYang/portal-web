package cn.com.bestpay.portal.service.api.index;


import cn.com.bestpay.portal.model.resp.ParentResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Service;

import java.util.concurrent.Future;

/**
 * Created by Howell on 5/12/16.
 */
@Service
@Slf4j
public class IndexService {

    @Async
    public Future<ParentResp> indexMain() throws InterruptedException{
        ParentResp parentResp = new ParentResp();
        parentResp.setCode("000000");
        parentResp.setContent("成功");
        log.info("成功");
        return new AsyncResult<ParentResp>(parentResp);
    }
}
