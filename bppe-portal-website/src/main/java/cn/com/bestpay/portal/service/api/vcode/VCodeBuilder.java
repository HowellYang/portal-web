package cn.com.bestpay.portal.service.api.vcode;

import cn.com.bestpay.portal.model.pojo.UtilsModel.VCodeModel;

public interface VCodeBuilder {
    public VCodeModel generate();
    public VCodeModel generate(int width, int height);
}
