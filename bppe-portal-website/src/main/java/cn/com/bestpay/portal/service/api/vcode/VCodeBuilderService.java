package cn.com.bestpay.portal.service.api.vcode;
import cn.com.bestpay.portal.model.pojo.UtilsModel.VCodeModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by Howell on 15/2/17.
 * 登录验证码图片
 */
@Service
@Slf4j
public class VCodeBuilderService implements VCodeBuilder, java.io.Serializable{
    private int width = 108; // 验证码图片的宽度
    private int height = 39; // 验证码图片的高度
    private int codeCount = 4; // 验证码字符个数
    private final int MIXED_LINE_COUNT = 20; // 干扰线的数量
    private final int GAP = 8;
    // private static final char[] CHAR_SEQ = { 'a', 'b', 'c', 'd', 'e', 'f',
    // 'g',
    // 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
    // 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
    // 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    // 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6',
    // '7', '8', '9' };
    /*
     * private static final char[] CHAR_SEQ = { 'a', 'b', 'c', 'd', 'e', 'f',
     * 'g', 'h', 'i', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
     * 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
     * 'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
     * 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
     */
    // 去掉字符i\o\I\O字母,去掉1数字
    private final char[] CHAR_SEQ = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p', 'q', 'r',
            's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N',
            'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '2', '3', '4', '5', '6', '7', '8', '9'};

    public VCodeBuilderService() {
    }

    /**
     * 使用固定长宽
     */
    public VCodeModel generate() {
        return createVcCode(width, height);
    }

    /**
     * 自定义
     */
    public VCodeModel generate(int width, int height) {
        return createVcCode(width, height);
    }

    /**
     * @Title: createVcCode
     * @Description: 生成二维码图片
     * @param @param width
     * @param @param height
     * @param @return
     * @date 2014年11月14日 下午6:16:47
     * @return VCodeModel
     * @throws
     */
    private VCodeModel createVcCode(int width, int height) {
        BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g2d = image.createGraphics();

        g2d.setColor(Color.WHITE); // 将图像填充为白色
        g2d.fill(new Rectangle2D.Double(0, 0, width, height));

        g2d.setFont(new Font("Times New Roman", Font.PLAIN, height - 2)); // 设置字体，字体的大小应该根据图片的高度来定
        g2d.setColor(Color.BLACK);
        // g2d.draw(new Rectangle2D.Double(0, 0, width - 1, height - 1));
        // g2d.setColor(getRandColor(180, 250));
        // g2d.fillRect(0, 0, width, height);

        // 随机产生干扰线，使图象中的认证码不易被其它程序探测到
        for (int i = 0; i < MIXED_LINE_COUNT; i++) {
            int startX = randInRange(1, width - 1);
            int startY = randInRange(1, height - 1);

            int endX = randInRange(1, width - 1);
            int endY = randInRange(1, height - 1);

            g2d.setColor(getRandColor(180, 250));
            g2d.drawLine(startX, startY, endX, endY);
        }

        g2d.setColor(Color.BLACK);
        char[] vCode = new char[codeCount]; // vCode - 随即生成的验证码
        int charWidth = width / (codeCount + 1);
        for (int i = 0; i < codeCount; i++) {
            vCode[i] = getNextChar();
            g2d.setColor(getRandColor(10, 150));
            g2d.drawString("" + vCode[i], i * charWidth + GAP, height - 4);
        }

        VCodeModel result = new VCodeModel(String.valueOf(vCode), image);
        //log.info("[generate]: Verify code created - " + result.getValue());
        return result;
    }

    // 给定范围获得一个随机颜色
    public static Color getRandColor(int fc, int bc) {
        Random random = new Random();
        if (fc > 255)
            fc = 255;
        if (bc > 255)
            bc = 255;
        int r = fc + random.nextInt(bc - fc);
        int g = fc + random.nextInt(bc - fc);
        int b = fc + random.nextInt(bc - fc);
        return new Color(r, g, b);
    }

    public char getNextChar() {
        return CHAR_SEQ[randInRange(0, CHAR_SEQ.length)];
    }

    public int randInRange(int x, int y) {
        return x + (int) (Math.random() * (y - x));
    }

    public double randInRange(double x, double y) {
        return x + Math.random() * (y - x);
    }
}
