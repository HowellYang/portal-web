/**
 * Created by Howell on 2016/9/21.
 * Email:th15817161961@gmail.com
 * 模块菜单路由配置
 */
define('subclass',["bestpay.app","bestpay.ui"],function(app, UI) {
    this.indexMainSelf = null;
    function IndexMainAction() {
        indexMainSelf = this;
        config.loadingDialog = UI.LoadingDialog();
        config.loadingDialog.showLoading();

        window.eval(" {{SelectRoute}} ");

        this.initApp();
    }

    //登录后菜单路由配置
    IndexMainAction.prototype.IndexRoute = function(){

        //一级菜单
        var headerMenuJson = [{
            "key":"Index","url":"/Index","templateUrl":"/Index/index/index.hbs"
        },{
            "key":"Pay","url":"/Pay","templateUrl":"/Pay/index/index.hbs"
        },{
            "key":"Finances","url":"/Finances","templateUrl":"/Finances/index/index.hbs"
        },{
            "key":"Inquiry","url":"/Inquiry","templateUrl":"/Inquiry/index/index.hbs"
        },{
            "key":"Account","url":"/Account","templateUrl":"/Account/index/index.hbs"
        }];
        BestpayApp.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
            console.log("init index router config");
            for(var i=0; i<headerMenuJson.length; i++){
                var item = headerMenuJson[i];
                $stateProvider.state(item["key"], {
                    url:"^/{parent}",
                    templateUrl:function($stateParams){
                        var parent = $stateParams["parent"];
                        return "&CDN_Url&/"+parent+"/index/index.hbs?v=&version&";
                    },
                    controller:['$scope', '$location', '$css','$stateParams', function($scope,$location,$css,$stateParams) {
                        var parent = $stateParams["parent"];
                        $css.bind("&CDN_Url&/"+parent+"/index/css/index.css?v=&version&",$scope);
                        //调用Header，显示菜单
                        indexMainSelf.SetMenuHeader(parent,$scope);
                    }]
                });
            }
            $urlRouterProvider.otherwise('/Index');
        }]);

        //二级菜单， 配置pay菜单路由
        var SecondLevelMenuList = [{
            "key":"phonerechargecard","url":"/phonerechargecard","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"SDMpay","url":"/SDMpay","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"PhonePay","url":"/PhonePay","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"Order","url":"/Order","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"Remuneration","url":"/Remuneration","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"RevenueExpenditure","url":"/RevenueExpenditure","templateUrl":"","parent":"Pay","group":""
        },{
            "key":"orderInquiry","url":"/orderInquiry","templateUrl":"","parent":"Inquiry","group":""
        }];
        BestpayApp.config(['$stateProvider', '$urlRouterProvider',function($stateProvider, $urlRouterProvider) {
            console.log("init pay router config");
            for(var i=0; i<SecondLevelMenuList.length; i++){
                var item = SecondLevelMenuList[i];
                $stateProvider.state(item["parent"]+"."+item["key"], {
                    url:"^/{parent}/{key}",
                    templateUrl: function($stateParams){
                        var parent = $stateParams["parent"],key = $stateParams["key"];
                        return "&CDN_Url&/"+parent+"/"+key+"/"+key+".hbs?v=&version&";
                    },
                    controller:['$scope','$css','$stateParams','$state', function($scope,$css,$stateParams,$state) {
                        var parent = $stateParams["parent"],key = $stateParams["key"];
                        $css.bind("&CDN_Url&/"+parent+"/"+key+"/css/"+key+".css?v=&version&",$scope);
                        $state.itemClass.initApp();
                    }],
                    resolve:{
                        load: ['$q','$stateParams','$state', function($q,$stateParams,$state) {
                            var defer = $q.defer();
                            console.info("$stateParams:"+$stateParams);
                            var parent = $stateParams["parent"],key = $stateParams["key"];
                            /* 动态加载angular模块 */
                            require(["&CDN_Url&/"+parent+"/"+key+"/js/"+key+".js?v=&version&"], function(itemClass) {
                                defer.resolve();
                                $state.itemClass = itemClass;
                            });
                            return defer.promise;
                        }]
                    }
                });
            }

        }]);
    };


    //登录前路由配置
    IndexMainAction.prototype.LoginRoute = function () {
        BestpayApp.controller('Outside', ['$scope', '$location', '$css', function($scope,$location,$css) {
            var path = $location.path();
            console.log(path);
            //调用Header，显示菜单
            indexMainSelf.SetOutsideHeader(path,$scope);

            if("/registration" != path){
                $css.bind("&CDN_Url&"+"/Outside"+path+"/css"+path+".css?v=&version&",$scope);
                require(["&CDN_Url&"+"/Outside"+path+"/js"+path+".js?v=&version&"],function(OutsideItemClass){
                    OutsideItemClass.initApp();
                });
            }
        }]);

        BestpayApp.config(['$stateProvider', '$urlRouterProvider', '$requireProvider' ,function($stateProvider, $urlRouterProvider, $requireProvider) {
            console.log("init index router config");
            var requireJS = $requireProvider.requireJS;
            var requireCSS = $requireProvider.requireCSS;

            $stateProvider.state('Login', {//登录
                url:"/Login",
                templateUrl: "&CDN_Url&/Outside/Login/Login.hbs?v=&version&",
                controller:"Outside"
            }).state('registration', {//注册
                url:"/registration",
                templateUrl: "&CDN_Url&/Outside/registration/registration.hbs?v=&version&",
                controller:"Outside",
                resolve:{
                    deps: requireJS([
                        "&CDN_Url&/Outside/registration/js/registration.js"
                    ]),
                    css: requireCSS([
                        "style!&CDN_Url&/Outside/registration/css/registration"
                    ])
                }
            }).state('forgetLoginPass', {//忘记密码
                url:"/forgetLoginPass",
                templateUrl: "&CDN_Url&/Outside/forgetLoginPass/forgetLoginPass.hbs?v=&version&",
                controller:"Outside"
            }).state('introduction', {//交费易介绍
                url:"/introduction",
                templateUrl: "&CDN_Url&/Outside/introduction/introduction.hbs?v=&version&",
                controller:"Outside"
            }).state('product', {//产品介绍
                url:"/product",
                templateUrl: "&CDN_Url&/Outside/product/product.hbs?v=&version&",
                controller:"Outside"
            }).state('question', {//常见问题
                url:"/question",
                templateUrl: "&CDN_Url&/Outside/question/question.hbs?v=&version&",
                controller:"Outside"
            }).state('appDownload', {//app下载
                url:"/appDownload",
                templateUrl: "&CDN_Url&/Outside/appDownload/appDownload.hbs?v=&version&",
                controller:"Outside"
            });
            $urlRouterProvider.otherwise('/Login');
        }]);
    };


    IndexMainAction.prototype.initApp = function () {
        console.log("MainAction init!");
        $(".common-body-style").show();
        config.loadingDialog.hideLoading();
    };

    IndexMainAction.prototype.SetMenuHeader = function (path,$scope) {
        require(["bestpay.header"],function (header) {
            header.SelectItem(path);
        });
        $scope.message = "1111";
        config.loadingDialog.hideLoading();
    };

    IndexMainAction.prototype.SetOutsideHeader = function (path,$scope) {
        require(["bestpay.outside.header"],function (header) {
            header.SelectItem(path);
        });
        config.loadingDialog.hideLoading();
    };

    return new IndexMainAction();
});
