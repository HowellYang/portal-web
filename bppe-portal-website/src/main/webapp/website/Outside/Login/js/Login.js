/**
 * Created by Howell on 16/11/16.
 */
/**
 * Created by Howell on 2/10/16.
 */
define(["bestpay.ui"],function(UI) {
    function IndexAction() {

    }

    IndexAction.prototype.initApp = function(){
        var self = this;
        console.log("init LoginAction");
        self.setVCode();
        $("#msg_form").show();
        //密码控件 init
        var LoginPassObj = UI.InputObject({"ID":"loginpwd-show", "Type":"password"});

        //按钮事件
        $("#GetPwd").click(function () {
            $("#id_login_loading").show();
            var pass_id = "loginpwd";
            UI.getPasswordSetSk(pass_id, function () {
                UI.getPasswordResult(pass_id, function (result) {
                    UI.getPasswordMachineNetwork(pass_id, function (network) {
                        UI.getPasswordMachineCPU(pass_id, function (cpu) {
                            UI.getPasswordMachineDisk(pass_id, function (disk) {
                                $("#machineNetwork").val( network);
                                $("#loginpwd").val( result );
                                $("#machineCPU").val( cpu );
                                $("#machineDisk").val( disk );
                                $("#id_submit").click();
                            });
                        });
                    });
                });
            });
        });

        $("#id-vcode-img").click(function () {
            self.setVCode();
        });
    };




    IndexAction.prototype.setVCode = function () {
        $("#id-vcode-img").attr("src","/api/vcode?vCodeHeight=48&vCodeWidth=102&ran" + Math.floor(Math.random() * 100 + 1) + '=' + Math.random());
    };

    return new IndexAction();
});