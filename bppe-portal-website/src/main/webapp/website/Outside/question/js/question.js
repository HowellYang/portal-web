/**
 * Created by Howell on 13/2/17.
 */
define(["bestpay.app"],function (app) {


    var Question = function () {
        this.questionlist = {"questionlist" : [
                {"classification":"注册开户","questions":[
                    {"title":"如何批量开通“交费易”？","answers":"目前翼支付账户批开，只接受省市电信公司/代理商申请。<br/>一、普通商户批开<br/>请提供：《交费易批量开户申请表》（excel） + 盖有公章的《交费易代理开户申请书V1.1》拍照件 ； <br/>二、连锁商户批开（适合总管帐号“已认证”的企业商户） <br/>请提供：《交费易批量开户申请表》（excel） + 盖有公章的《交费易代理开户申请书V1.1》拍照件 + 成员的身份证正反面拍照件；<br/> ——请严格按照以上说明提供齐全的资质材料，发送至交费易payeasy@bestpay.com.cn 公共邮箱处理，资质不全不能予以受理。<br/>——来件请说明发件人、省市、公司、联系方式、详细需求。由省（市）电信接口人或代理商发送申请。<br/>——邮件处理时间：三个工作日内处理完成。"}
                ]},
                {"classification":"绑卡","questions":[
                    {"title":"为什么绑卡失败？","answers":"出现这种情况的可能原因：<br/>①商户的注册资料与银行方的银行卡登记信息不符；<br/>②银联系统资料库调用信息有误引起。<br/>处理方式：<br/>（1）商户通过客服核实注册信息是否有误（申请人姓名、身份证号），如有即申请修改；<br/>（2）商户自行与银行方联系确认信息，如确认信息与“交费易”注册信息无误的情况下，目前暂时只能更换其他银行卡进行绑卡操作。"},
                    {"title":"支持绑定哪些银行卡？","answers":"目前交费易支持在线绑卡的银行有：<br/>中国银行、工商银行、农业银行、建设银行、邮储银行、兴业银行、光大银行、平安银行、上海银行、中信银行、招商银行。<br/>"},
                    {"title":"如何更换银行卡？","answers":"一、更换签约授权银行账户操作方式如下：<br/>①交费易门户官网https://b.bestpay.com.cn：账户管理→银行卡→输入支付密码→点击“管理”→删除→解除绑定银行卡→再重新绑定需要更换的签约授权银行卡；<br/>②交费易手机App：账户中心→银行卡→点击已绑定银行卡→解绑→确定→再重新绑定需要更换的签约授权银行卡；<br/>【温馨提示】解绑和绑定银行卡一天只能操作一次。<br/>二、更换注册手机号码操作方式如下：<br/>交费易门户官网https://b.bestpay.com.cn：账户管理→个人设置→更换手机→输入短信验证码、需更换的手机号码→确定；（该操作适用于能接收短信验证码的步骤）<br/>如不能通过上述方法找回，请联系客服客服处理。联系客服方式：<br/>方式一：登录交费易手机app，点击右上方“服务”--“客服”--自动转接在线客服；<br/>方式二：登录 https://b.bestpay.com.cn，点击右下方“在线客服”；<br/>方式三：拨打客服热线4008011888。 "}
                ]},
                {"classification":"密码","questions":[
                    {"title":"忘记登录密码怎么办？","answers":"可通过“忘记密码”功能申请找回登录密码即可。<br/>操作路径：在登录页面，点击“忘记密码”→输入“登录号”→点击下一步→输入短信验证码、身份证号码验证、新密码，点击确定→系统返回结果。<br/>如不能通过上述方法找回，请联系客服客服处理。联系客服方式：<br/>方式一：登录交费易手机app，点击右上方“服务”--“客服”--自动转接在线客服；<br/>方式二：登录交费易门户网站b.bestpay.com.cn，点击右下方“在线客服”；<br/>方式三：拨打客服热线4008011888。 "},
                    {"title":"忘记支付密码怎么办？","answers":"可通过以下两种方式自助重置支付密码：<br/>①：交费易门户b.bestpay.com.cn，进行账户登录，然后进入账户管理→支付密码→忘记支付密码→填写验证信息→下一步→输入新密码→系统返回结果。；<br/>②：手机客户端登录→账户中心→密码管理→找回支付密码→填写验证信息→下一步→输入新密码→系统返回结果。<br/>若商户未绑定银行卡，系统将提示商户先绑定银行卡再找回支付密码。<br/>如不能通过上述方法找回，请联系客服客服处理。联系客服方式：<br/>方式一：登录交费易手机app，点击右上方“服务”--“客服”--自动转接在线客服；<br/>方式二：登录 https://b.bestpay.com.cn，点击右下方“在线客服”；<br/>方式三：拨打客服热线4008011888。"},
                    {"title":"密码被锁了怎么办？","answers":"如需解锁登录密码、支付密码，可通过重置密码，重置成功后自动解锁，也可以联系客服，客服人员核实身份信息后会帮助处理。<br/>联系客服方式：<br/>方式一：登录 https://b.bestpay.com.cn，点击右下方“在线客服”；<br/>方式二：拨打客服热线4008011888。"}
                ]},
                {"classification":"酬金","questions":[
                    {"title":"什么是酬金？酬金如何结算？","answers":"酬金是指商户为顾客提供各类便民服务后可获取的业务酬金。例如商户您为顾客充值100元的电信手机话费，您将得到1.6元的业务酬金，酬金返还至您的余额中。<br/>非企业用户：酬金直接发放到账户余额中，无需结转，自动扣税；<br/>企 业 用 户 ：酬金直接发放到账户的冻结余额中，无扣税，每月开具发票后工作人员为您解冻，无需结转；"},
                    {"title":"企业商户的酬金是冻结状态，该如何处理？","answers":"企业商户交易酬金是冻结的，是由于企业商户酬金为“月结”形式，所以酬金为冻结状态。<br/>如需解冻酬金，需按照流程进行申请结转：<br/>①.【导出酬金报表】：系统每月6日生成上月酬金报表，届时，商户可登陆“交费易”门户网站：https://b.bestpay.com.cn/→选择““交费易””→选择“账户中心”—“酬金统计”→选择需要查询的日期，点击“查询”→点击“查看明细”→点击“导出”，导出酬金报表excel表格。<br/>②.商户按月打印酬金报表，签字并加盖公章，并按报表中显示的酬金总额自行前往当地税务局开具等额发票，把报表与发票一同寄到翼支付公司处理。（注意：报表中的金额是价税合计的金额。）<br/>发票种类：1、增值税普通发票（酬金所属期是2016年4月30日前）； 2、酬金所属期是2016年5月1日后，一律开具增值税专用发票，小规模纳税人请到当地税局，由税局代开增值税专用发票；<br/>发票内容：建议写“代理服务费”、“服务费”，或商户自行去当地税局了解充值业务可开具的发票内容；<br/>发票抬头：天翼电子商务有限公司广东分公司；<br/>纳税人识别号：国税号：440100579951525；<br/>地址：广州市天河区花城大道18号建滔广场20楼 <br/>联系电话：020-37594020；<br/>银行账户：44001400101053013931；<br/>开户行：建设银行广州越秀支行；<br/>发票盖章：盖发票专用章，盖章公司名称必须与注册时登记的商户名称一致；<br/>温馨提示：为保障商户利益，发票金额请务必与报表酬金总计金额一致。多份未结算月份的酬金报表，酬金可合计开具一张发票，发票金额需按照年份开具，报表需按月份开打印。<br/>③.支付公司收到商户寄来的报表和发票，会按正常结算流程处理，为商户解冻并结转酬金至交费易账户。               "},
                    {"title":"代理商的服务酬金是如何计算的？","answers":"代理商的服务酬金包括：网点拓展奖励和交易提成奖励两部分。①网点拓展奖励结算方式：在本代理商所属有效网点，发展的新商户在前6个月内交费易账户消费累计金额大于等于2000元为有效网点，奖励代理商100元/每有效网点；<br/>②交易提成奖励结算方式：本代理商所拓展网点可计提酬金业务的总体月成功交易金额（区域内） * 相应业务的酬金费率（具体可计提的业务列表和酬金费率详细见代理商合同附件2）"},
                    {"title":"企业商户、代理商酬金时间处理流程","answers":"系统每月8日左右生成代理商酬金报表，若“交费易”官方工作人员于每月15日之前收到您的酬金结算申请材料，则将从当月16日开始计算25个工作日内完成酬金发放；若“交费易”官方工作人员于每月15日之后（含每月15日）收到您的酬金结算材料，则将从次月1日开始计算35个工作日内完成酬金发放。（法定节假日顺延）"},
                    {"title":"通过“交费易”业务缴交电信费用，这部分收入集团会与本地网进行结算吗？","answers":"目前已和集团账务部确认，由于“交费易”的电信充值是调用集团的统一缴费接口的，而统一缴费接口是纳入集团一点结算的，因此不会影响省公司的现金流考核，具体可以联系集团核实。"}
                ]},
                {"classification":"交易问题","questions":[
                    {"title":"扣款成功交易失败怎么办？","answers":"答：如果业务界面显示交易失败，正常情况交费易会在24小时内退费至商户的交费易账户。<br/>若超过24小时仍未退款，用订单号去收支明细查退款情况，没有查询到请商户联系客服处理。"},
                    {"title":"便民业务交易后通过订单查询发现交易状态仍在“处理中”，怎么办？","answers":"答：交费易便民业务交易24小时内有效，所以，务必先收取顾客的交易费用，并需要在24小时后再次进行交易状态确认。如24小时后交易状态仍为“处理中”，请商户联系客服处理。"},
                    {"title":"商户提供便民业务服务提示“交易成功”，但没有酬金返还，怎么办？","answers":"答：正常情况下，酬金返还的时效为24小时，若24小时内仍无酬金返还，请商户联系客服处理。"}
                ]},
                {"classification":"账号问题","questions":[
                    {"title":"如何进行挂失（冻结）、解挂（解冻）账号的操作?","answers":"答：挂失（冻结）等业务是立即生效，请联系客服申请，按客服指引提交相关的资料，包括身份证（复印件）、更换的银行卡（复印件）、申请表等资料。"}
                ]},
                {"classification":"额度","questions":[
                    {"title":"如何自助提升额度？","answers":"商户可通过以下方式自行登录查看交易额度使用情况：<br/>①门户：https://b.bestpay.com.cn/，选择“账户管理--额度查询--星级额度”；<br/>②安卓客户端:选择“账户中心--额度管理”；<br/>③IOS客户端：选择“账户中心--额度管理”。<br/>如符合申请条件可点击“提升额度”自助提交申请。工作人员将在两个工作日内受理完成，请留意变更状态。"}
                ]}
            ]};
        this.initApp();
    };

    Question.prototype.initApp = function () {
        this.setClassification();
    };

    /**
     * 初始化问题分类选项
     */
    Question.prototype.setClassification = function() {
        var self = this;
        var questionlist = this.questionlist["questionlist"];
        var questionlistStr = "";
        for(var i = 0; i < questionlist.length; i++) {
            var classifica = questionlist[i];
            if(i == 0){
                questionlistStr += '<span class="select_classification">' + classifica["classification"] + '</span>';
                self.setQuestions(classifica["classification"]);
            } else {
                questionlistStr += '<span>' + classifica["classification"] + '</span>';
            }
        }
        $("#id_items").html(questionlistStr);
        $('#id_items span').each(function () {
            $(this).click(function(){
                $('#id_items .select_classification').removeClass("select_classification");
                $(this).addClass("select_classification");
                self.setQuestions($(this).text());
            });
        });

    };


    /**
     * 初始化问题标题
     * @param classification 分类名称
     */
    Question.prototype.setQuestions = function(classification){
        console.log(classification);
        var self = this;
        var questionlist = this.questionlist["questionlist"];
        var questionlistStr = "";
        var imgstr ='<img src="&CDN_Url&/Outside/question/img/icon_left.png?v=&version&">';
        var questions;
        for (var i = 0; i < questionlist.length; i++) {
            var classifica = questionlist[i];
            if(classifica["classification"] == classification){
                questions = classifica["questions"];
                for (var j =0; j < questions.length; j++){
                    var item = questions[j];
                    if(j == 0){
                        questionlistStr += '<div class="select_title">'+ imgstr + item["title"] + '</div>';
                        self.setQuestionItem(questions, item["title"]);
                    } else {
                        questionlistStr += '<div>' + item["title"] + '</div>';
                    }
                }
            }
        }
        $("#id_titles").html(questionlistStr);
        $('#id_titles div').each(function () {
            $(this).click(function(){
                $('#id_titles .select_title').html($('#id_titles .select_title').html().replace(imgstr,""));
                $('#id_titles .select_title').removeClass("select_title");
                self.setQuestionItem(questions, $(this).text());
                $(this).addClass("select_title");
                $(this).html(imgstr + $(this).html());
            });
        });
    };

    /**
     * 初始化问题内容
     * @param questions 分类问题list
     * @param title 问题title
     */
    Question.prototype.setQuestionItem = function (questions, title) {
        var answerstr = '<div class="answer_title">' + title + '</div>';
        for (var j =0; j < questions.length; j++){
            var item = questions[j];
            if( item["title"] == title){
                answerstr += item['answers'];
            }
        }
        $("#id_answers").html(answerstr);
    };
    return new Question();
});