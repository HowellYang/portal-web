/**
 * Created by Howell on 16/11/16.
 */
/**
 * Created by Howell on 4/10/16.
 */
define(["bestpay.app"],function(app) {
    function Registration() {
        this.initApp();
    }

    Registration.prototype.initApp = function () {
        this.initController();
        this.initDirective();
    };

    Registration.prototype.initController = function() {

        app.controller('question', ['$scope', function($scope) {
            $scope.nameStr = "Hi!Yang"
        }]);

        app.controller('SpicyController', ['$scope', function($scope) {
            $scope.customSpice = "wasabi";
            $scope.spice = 'very';
            $scope.spicy = function(spice) {
                $scope.spice = spice;
            };
        }]);

    };

    Registration.prototype.initDirective = function() {

    };


    return new Registration();
});