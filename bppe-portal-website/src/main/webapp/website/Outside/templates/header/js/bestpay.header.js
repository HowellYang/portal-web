/**
 * Created by Howell on 2016/9/21.
 * Email:th15817161961@gmail.com
 */

define('bestpay.outside.header',[],function () {
    var Header = function () {
        this.initApp();
    };

    Header.prototype.initApp = function () {
        this.initBtn();
    };

    Header.prototype.initBtn = function () {
        //显示公众号二维码
        var hideCode = function () {
            $("#id_header_webcode").hide();
        }, hideAction;
        $("#id_webc").bind("mouseenter", function() {
            $("#id_header_webcode").show();
            hideAction = hideCode;
        }).bind("mouseleave ", function() {
            setTimeout(function () {
                hideAction();
            },500);
        });
        $("#id_header_webcode").bind("mouseenter", function() {
            hideAction = function () {};
            $("#id_header_webcode").show();
        }).bind("mouseleave ", function() {
            hideAction = hideCode;
            hideAction();
        });

    };

    Header.prototype.SelectItem = function (path) {
        $('#index_menu .menu-select-out').removeClass("menu-select-out");
        $('#index_menu .menu-link-out').each(function () {
            var thisPath = $(this).attr("href").replace("#","");
            if(thisPath == path.replace("/","")){
                $(this).removeClass("menu-link-out");
                $(this).addClass("menu-select-out").addClass("menu-link-out");
            }
        });
    };

    Header.prototype.InjectJS = function(path){
    };

    return new Header();
});


