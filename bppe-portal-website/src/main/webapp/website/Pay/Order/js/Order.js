/**
 * Created by Howell on 23/11/16.
 */
define(["bestpay.http","bestpay.ui",'highcharts','highchartsExporting'],function(HTTP,UI,highcharts,highchartsExporting) {
    function Order() {

    }

    Order.prototype.initApp = function () {
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show')
        });
        this.Proportion_show();
        this.Surrounds_show();

        this.DownloadFile();

    };

    Order.prototype.DownloadFile = function () {
        var downloadObj = UI.ExportFile({
            "ID":"id_download_wrap",
            "Url":"/api/export/OrderXls",
            "Success":function (result) {
                if(result["code"] == "000000"){

                }
            }
        });
        $("#id_DownloadFile").click(function () {
           downloadObj.Submit();
        });
        var downloadBigObj = UI.ExportFile({
            "ID":"id_download_wrap",
            "data":{"fileName":"大文件数据"},
            "Url":"/api/export/OrderBigXls",
            "Success":function (result) {
                if(result["code"] == "000000"){

                }
            }
        });
        $("#id_DownloadBigFile").click(function () {
            downloadBigObj.Submit();
        });

    };



    Order.prototype.Proportion_show = function () {
        Highcharts.chart('Proportion_show', {
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: {
                text: '2016年占比'
            },
            exporting: {
                //enabled:true,默认为可用，当设置为false时，图表的打印及导出功能失效
                filename:'2016年占比'//导出的文件名
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                name: 'pay',
                colorByPoint: true,
                data: [{
                    name: '手机充值',
                    y: 56.33
                }, {
                    name: '固话宽带',
                    y: 20.03,
                    sliced: true,
                    selected: true
                }, {
                    name: '游戏充值',
                    y: 14.38
                }, {
                    name: '水电煤',
                    y: 2.77
                }, {
                    name: 'QQ充值',
                    y: 2.91
                }, {
                    name: '翼支付充值',
                    y: 0.2
                }]
            }]
        });
    };

    Order.prototype.Surrounds_show = function () {
        Highcharts.chart('Surrounds_show', {
            chart: {
                type: 'column'
            },
            title: {
                text: '2015年、2016年'
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                categories: [
                    '手机充值',
                    '固话宽带',
                    '游戏充值',
                    '水电煤',
                    'QQ充值',
                    '翼支付充值',
                    '卡卷购买',
                    '交通罚款',
                    '综合缴费'
                ],
                crosshair: true
            },
            yAxis: {
                min: 0,
                title: {
                    text: '金额 (元)'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} 元</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: '2015年',
                data: [1989, 715, 1064, 1292, 1440, 1760, 1356, 1485, 164]
            }, {
                name: '2016年',
                data: [1836, 788, 985, 934, 1060, 845, 1050, 1043, 92]

            }]
        });
    };

    return new Order();
});