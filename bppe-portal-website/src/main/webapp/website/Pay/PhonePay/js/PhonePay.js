/**
 * Created by Howell on 23/11/16.
 */
define(["bestpay.http","bestpay.ui"],function(HTTP,UI) {
    function PhonePay() {

    }

    PhonePay.prototype.initApp = function () {
        $("#id_model_wrap").show();
        //密码控件 init
        var PhoneNumberObj = UI.InputObject({"ID":"PhoneNumber", "Type":"phone"});
        var BackNumberObj = UI.InputObject({"ID":"BackNumber", "Type":"bankCard"});
        var IntNumberObj = UI.InputObject({"ID":"IntNumber", "Type":"number","Decimal":"2"});

        //上传文件
        $("#id_SelectFile").AjaxFileUpload({
            action :"/api/induction/dataFile",
            onComplete: function(filename, response) {
                alert(response);
            }
        });

    };

    return new PhonePay();
});