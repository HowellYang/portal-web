/**
 * Created by Howell on 23/11/16.
 */
define(["bestpay.http","bestpay.ui",'highcharts'],function(HTTP,UI,highcharts) {
    function RevenueExpenditure() {

    }

    RevenueExpenditure.prototype.initApp = function () {
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show')
        });
        this.Income_show();
        this.Disbursement_Show();

    };

    RevenueExpenditure.prototype.Disbursement_Show = function () {
        Highcharts.chart('Disbursement_show', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: '支出'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: [
                    "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
                ]
            },
            yAxis: {
                title: {
                    text: '金额(元)'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: '元'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [{
                name: '2015年',
                data: [3500, 2300, 5400, 6505, 5900, 4000, 2004, 3450, 7505, 8522, 8850, 6524]
            },{
                name: '2016年',
                data: [4100, 3200, 5400, 6505, 4040, 5004, 3522, 4850, 6524]
            }]
        });
    };

    RevenueExpenditure.prototype.Income_show = function () {
        Highcharts.chart('Income_show', {
            chart: {
                type: 'areaspline'
            },
            title: {
                text: '收入'
            },
            legend: {
                layout: 'vertical',
                align: 'left',
                verticalAlign: 'top',
                x: 150,
                y: 100,
                floating: true,
                borderWidth: 1,
                backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
            },
            xAxis: {
                categories: [
                    "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月", "十月", "十一月", "十二月"
                ]
            },
            yAxis: {
                title: {
                    text: '金额(元)'
                }
            },
            tooltip: {
                shared: true,
                valueSuffix: '元'
            },
            credits: {
                enabled: false
            },
            plotOptions: {
                areaspline: {
                    fillOpacity: 0.5
                }
            },
            series: [{
                name: '2015年',
                data: [300, 230, 500, 605, 900, 400, 204, 450, 505, 522, 850, 524]
            },{
                name: '2016年',
                data: [330, 240, 520, 505, 980, 420, 244, 452, 506]
            }]
        });
    };

    return new RevenueExpenditure();
});