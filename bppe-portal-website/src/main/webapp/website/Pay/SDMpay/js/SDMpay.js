/**
 * Created by Howell on 4/10/16.
 */
define(["bestpay.http","bestpay.ui",'bootstrapDatetimepicker','bootstrapDatetimepickerZhCN'],function(HTTP,UI) {
    function SDMpay() {

    }
    SDMpay.prototype.initApp = function () {
        $('.form_datetime').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            showMeridian: 1
        });
        $('.form_date').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
        $('.form_time').datetimepicker({
            language:  'zh-CN',
            weekStart: 1,
            todayBtn:  1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 1,
            minView: 0,
            maxView: 1,
            forceParse: 0
        });

        //密码控件
        var PayPassObj = UI.InputObject({"ID":"paypwd-show", "Type":"password"});

        HTTP.getDeviceInfo(function (params) {
            alert( JSON.stringify(params) );
        });

    };

    return new SDMpay();
});