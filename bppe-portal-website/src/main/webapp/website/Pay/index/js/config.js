/**
 * Created by Howell on 2016/9/21.
 * Email:th15817161961@gmail.com
 */

define({
    'pageName':'便民主页',
    'PayConfig':{
        'payMenu':[
            {
                'menuID':'0',
                'menuName':'充值卡',
                'menuSref':'.phonerechargecard',
                'menuUrl':'#/Pay/phonerechargecard'
            },{
                'menuID':'1',
                'menuName':'水电煤',
                'menuSref':'.SDMpay',
                'menuUrl':'#/Pay/SDMpay'
            }]
    }
});