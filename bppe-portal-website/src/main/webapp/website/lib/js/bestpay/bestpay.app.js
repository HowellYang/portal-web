/**
 * Created by Howell on 2016/9/25.
 * Email:th15817161961@gmail.com
 * 公共js: angular init
 */
define("bestpay.app",["angular","angularUIRouter","jquery","angularCSS",'angularRequire',"bootstrap","Base64","crypto","PassGuardCtrl","requireCss"], function() {

    var app = angular.module('bestpay.app', ['ui.router', 'ngRequire', 'angularCSS']);
    app.config(["$sceDelegateProvider",function ($sceDelegateProvider) {
        // $sceDelegateProvider.resourceUrlWhitelist([
        //     // Allow same origin resource loads.
        //     'self'
        // ]);
    }]);
    window.BestpayApp = app;

    return app;
});

