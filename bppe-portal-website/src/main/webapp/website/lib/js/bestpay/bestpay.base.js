/**
 * Created by Howell on 2016/9/21.
 * Email:th15817161961@gmail.com
 * 公共js: 前端js入口，配置require
 *
 */
var _url = window.location.href,
    _subclass = _url.substring(0, _url.indexOf('.html')+5);
    _subclass = _subclass.substring(_subclass.lastIndexOf('/') + 1, _subclass.indexOf('.html'));
function getRandNumber() {
    var rand = Math.round(Math.random()*899999+100000);
    return rand;
}

// 浏览器调试安全提示！
!function(e){function o(s){if(t[s])return t[s].exports;var n=t[s]={exports:{},id:s,loaded:!1};return e[s].call(n.exports,n,n.exports,o),n.loaded=!0,n.exports}var t={};return o.m=e,o.c=t,o.p="",o(0)}([function(e,o,t){e.exports=t(1)},function(e,o){!function(){var e;if(window.console&&"undefined"!=typeof console.log){try{(window.parent.__has_console_security_message||window.top.__has_console_security_message)&&(e=!0)}catch(o){e=!0}if(window.__has_console_security_message||e)return;var t=" 温馨提示：请不要调皮地在此粘贴执行任何内容，这可能会导致您的账户受到攻击，给您带来损失 ！._.",s="^_^",n="",i=[s," ",n].join("");/msie/gi.test(navigator.userAgent)?(console.log(t),console.log(i)):(console.log("%c 交费易 %c Copyright \xa9 2011-%s",'font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;font-size:64px;color:#ec7500;-webkit-text-fill-color:#ec7500;-webkit-text-stroke: 1px #ec7500;',"font-size:12px;color:#999999;",(new Date).getFullYear()),console.log("%c "+t,"color:#333;font-size:16px;"),console.log("\n "+i)),window.__has_console_security_message=!0}}()}]);

// 判断 IE7 浏览器，跳转到提示升级浏览器的页面
if(document.querySelector == null){
    location.href = "/static/UpdateBrowser.html";
}

// 处理IE8，Array不支持indexOf的方法。
if (!Array.prototype.indexOf){
    Array.prototype.indexOf = function(elt /*, from*/){
        var len = this.length >>> 0;
        var from = Number(arguments[1]) || 0;
        from = (from < 0)
            ? Math.ceil(from)
            : Math.floor(from);
        if (from < 0)
            from += len;
        for (; from < len; from++) {
            if (from in this &&
                this[from] === elt)
                return from;
        }
        return -1;
    };
}

require.config({
    baseUrl : "js",
    waitSeconds: 0,
    paths : {
        'bestpay.app' : '&CDN_Url&/lib/js/bestpay/bestpay.app',
        'bestpay.lang' : '&CDN_Url&/lib/js/bestpay/bestpay.lang',
        'bestpay.ui' : '&CDN_Url&/lib/js/bestpay/bestpay.ui',
        'bestpay.http' : '&CDN_Url&/lib/js/bestpay/bestpay.http',
        'bestpay.global' : '/lib/js/bestpay/bestpay.global',
        'bestpay.header':'&CDN_Url&/lib/templates/header/js/bestpay.header',
        'bestpay.outside.header':'&CDN_Url&/Outside/templates/header/js/bestpay.header',

        'Base64' : '&CDN_Url&/lib/js/SecurityPassword/Base64',
        'crypto' : '&CDN_Url&/lib/js/SecurityPassword/crypto-js',
        'PassGuardCtrl' : '&CDN_Url&/lib/js/SecurityPassword/PassGuardCtrl',

        'jquery' : '&CDN_Url&/lib/js/thirdParty/jquery-1.12.4.min',
        'jqueryDataTables' : '&CDN_Url&/lib/js/thirdParty/jquery.dataTables.min',
        'angular' : '&CDN_Url&/lib/js/thirdParty/angular.min',
        'angularUIRouter': '&CDN_Url&/lib/js/thirdParty/angular-ui-router.min',
        'angularCSS' : '&CDN_Url&/lib/js/thirdParty/angular-css.min',
        'angularRequire': '&CDN_Url&/lib/js/thirdParty/angular-require.min',
        'requireCss':'&CDN_Url&/lib/js/thirdParty/require-css.min',

        'header.router' : '&CDN_Url&/lib/templates/header/js/header.router',
        'bootstrap':'&CDN_Url&/lib/js/thirdParty/bootstrap.min',
        'bootstrapDatetimepicker':'&CDN_Url&/lib/js/thirdParty/bootstrap-datetimepicker.min',
        'bootstrapDatetimepickerZhCN':'&CDN_Url&/lib/js/thirdParty/bootstrap-datetimepicker.zh-CN',
        'highcharts':'&CDN_Url&/lib/js/thirdParty/highcharts.min',
        'highchartsExporting' : '&CDN_Url&/lib/js/thirdParty/highcharts.exporting.min',

        // 子应用特有
        'subconfig' : 'config',
        'subclass'  : _subclass
    },
    shim: {
        angular: ['jquery'],
        angularUIRouter: ['angular'],
        angularCSS: ['angular'],
        angularRequire:['angular'],
        highcharts:['jquery'],
        highchartsExporting:['highcharts'],
        bootstrap:['angular','jquery'],
        PassGuardCtrl:['jquery','crypto'],
        jqueryDataTables:['jquery'],
        bootstrapDatetimepicker:['bootstrap','jquery'],
        bootstrapDatetimepickerZhCN:['bootstrap','jquery','bootstrapDatetimepicker']
    },
    urlArgs : function (id, url) {
        var args = "v=&version&";
        if( id == 'jquery' ||
            id == 'jqueryDataTables' ||
            id == 'angular' ||
            id == 'highcharts' ||
            id == 'angularUIRouter' ||
            id == 'bootstrap' ||
            id == 'angularCSS' ||
            id == 'bootstrapDatetimepicker' ||
            id == 'bootstrapDatetimepickerZhCN' ||
            id == 'Base64' ||
            id == 'PassGuardCtrl' ||
            id == 'highchartsExporting'
        ){
            args = "v=1.0.0";
        }
        return (url.indexOf('?') === -1 ? '?' : '&') + args;
    }
});

if ('&debug&' != 'true') {
    console.log = function () {};
    console.info = function () {};
    console.error = function() {};
}
console.log("init : "+_subclass);

'use strict';
require(['bestpay.global','subconfig'], function(config, subconfig) {
    // 将模块中的配置覆盖全局配置
    for (var prop in subconfig) {
        config[prop] = subconfig[prop];
    }
    window.config = config;
    require(['subclass'], function(subclass) {

        //由 "subclass" 去渲染路由配置
        BestpayApp.config(['$cssProvider',function ($cssProvider) {
            angular.extend($cssProvider.defaults, {
                container: 'head',
                method: 'append',
                persist: true,
                preload: true,
                bustCache: false
            });
        }]);

        //渲染
        angular.bootstrap(document, ['bestpay.app']);
    });
});



