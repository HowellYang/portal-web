/**
 * Created by Howell on 2016/9/30.
 * Email:th15817161961@gmail.com
 * 公共js:全局配置
 */

'[nocompress]';

define({
    "injections":{{injections}},
    "passOnBlur":function(){},
    "passOnFocus":function(){}
});