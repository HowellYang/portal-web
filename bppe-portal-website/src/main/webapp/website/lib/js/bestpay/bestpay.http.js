/**
 * Created by Howell on 2016/9/25.
 * Email:th15817161961@gmail.com
 * 公共js: 公共数据交互
 */
define(['bestpay.lang',"bestpay.ui","Base64","PassGuardCtrl"],function(Lang, UI) {
    function Http() {

    }
    Http.prototype.initApp = function () {

    };


    Http.prototype.setCommonParams = function (params) {
        params.WebKeep = Lang.getKeep();

        return params;
    };

    Http.prototype.getSecurityScriptRD = function () {
        var SecurityScriptRD = "";
        this.callWebService({
            'service': '/api/security/random',
            'params': {},
            'showLoading': false,
            'async':false,
            'success': function(result){SecurityScriptRD =result['SecurityScriptRD'];}
        });
        return SecurityScriptRD;
    };

    /**
     * 获取用户使用的设备的硬件信息
     * @returns {{MachineNetwork: *, MachineDisk: *, MachineCPU: *}}
     */
    Http.prototype.getDeviceInfo = function (callf) {
        var obj = new $.pge({pgeClass:"hiddenPWD",pgeId: "SecurityScript-self","pgeWindowID":"password"+new Date().getTime()});
        var divObj = document.createElement("div");
        divObj.id = "_id_SecurityScript_Show";
        document.body.appendChild(divObj);

        var SecurityScriptRD = this.getSecurityScriptRD();

        if(obj.osBrowser == 10 || obj.osBrowser == 11){
            obj.pgInitialize();
            obj.instControl(obj.settings.pgeWindowID, function () {
                obj.pwdSetSk(SecurityScriptRD, function() {
                    obj.openProt(obj.settings.pgeWindowID, obj.settings.pgeId);
                    obj.machineNetwork( function(network) {
                        obj.machineDisk( function(disk) {
                            obj.machineCPU( function(cpu) {
                                if(network.code == 0 && disk.code == 0 && cpu.code == 0 && callf != null){
                                    obj.closeProt(obj.settings.pgeWindowID,obj.settings.pgeId);
                                    callf({"MachineNetwork" : BASE64.encoder(network.data),
                                        "MachineDisk" : BASE64.encoder(disk.data),
                                        "MachineCPU" : BASE64.encoder(cpu.data) });
                                }
                            });
                        });
                    });
                });
            });
        } else {
            obj.generate("_id_SecurityScript_Show");
            obj.pgInitialize();
            obj.pwdSetSk(SecurityScriptRD);
            var machineNetwork = BASE64.encoder(obj.machineNetwork());
            var machineDisk = BASE64.encoder(obj.machineDisk());
            var machineCPU = BASE64.encoder(obj.machineCPU());

            callf({
                "MachineNetwork" : machineNetwork,
                "MachineDisk" : machineDisk,
                "MachineCPU" : machineCPU
            });
        }
        document.body.removeChild(divObj);
        return null;
    };



    /**
     * Ajax call to Web Service
     * @param settings 字段：
     * {
     *  service : 请求地址,
     *  params : 请求参数,
     *  success :,
     *  error :,
     *  showLoading :,
     *  setTime :,
     *  async :,
     *  openInject :
     * }
     */
    Http.prototype.callWebService = function(settings){
        config.isOpen = true;
        var callSelf = this;
        var url = null;
        if (config.loadingDialog == null){
            config.loadingDialog = UI.LoadingDialog();
        }
        if (settings.showLoading != null && settings.showLoading == true) {
            config.loadingDialog.showLoading();
        }

        if(settings.setTime == null){
            settings.setTime = 75000;
        }

        if(settings.openInject == null){
            settings.openInject = false;
        }

        if (typeof settings.error !== 'function') {
            settings.error = function () {
                console.log("error 请求失败: " + url);
            }
        }
        var successCallBack = function(result) {
            if (settings.showLoading != null && settings.showLoading == true) {
                config.loadingDialog.hideLoading();
            }
            console.log('successCallBack response: ' + JSON.stringify(result));
            config.isOpen = true;
            if(settings.openInject){
                var global_url = '/lib/js/bestpay/bestpay.global.js?v=' + Date.parse(new Date());
                require([global_url],function(config){
                    for (var prop in config) {
                        window.config[prop] = config[prop];
                    }
                    settings.success(result);
                });
            } else {
                settings.success(result);
            }
        };


        var errorCallBack = function(result){
            if (settings.showLoading != null && settings.showLoading == true) {
                config.loadingDialog.hideLoading();
            }
            console.log('successCallBack response: ' + JSON.stringify(result));
            config.isOpen = true;

            settings.error(result);
        };

        var suffix = '?ran' + Math.floor(Math.random() * 100 + 1) + '=' + Date.parse(new Date());

        if(settings.URL != null){
            url = settings.URL + settings.service + suffix;
        } else {
            url = settings.service + suffix;
        }
        if(settings.async == null){
            settings.async = true;
        }
        console.log('isOpen: ' + config.isOpen);
        if(config.isOpen == false){
            return;
        }else{
            config.isOpen = false;
        }
        console.log('url: ' + url);

        var ActionCall = function () {
            try {
                if (typeof settings.params === 'object') {
                    settings.params = JSON.stringify(settings.params);
                }
                console.log('request: ' + settings.params);

                window.jqXHR = $.ajax({
                    url: url,
                    type: "POST",
                    async: settings.async,
                    data: settings.params,
                    timeout: settings.setTime,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: successCallBack,
                    error: errorCallBack
                });
            } catch (e) {
                console.error('try catch error: ' + e.message);
                config.isOpen = true;
                settings.error({"msg":e.message});
            }
        };

        if(settings.params["isCheckDeviceInfo"] != null && settings.params["isCheckDeviceInfo"] == "Y"){
            callSelf.getDeviceInfo(function(params){
                //硬件信息
                settings.params["MachineNetwork"] = params['MachineNetwork'];
                settings.params["MachineDisk"] = params['MachineDisk'];
                settings.params["MachineCPU"] = params['MachineCPU'];
                ActionCall();
            });
        } else {
            ActionCall();
        }

    };


    return new Http();
});