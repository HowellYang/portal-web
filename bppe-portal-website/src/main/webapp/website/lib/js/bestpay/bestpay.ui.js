/**
 * Created by Howell on 2016/9/25.
 * Email:th15817161961@gmail.com
 * 公共js: 公共组件
 */
define(["highcharts","Base64","PassGuardCtrl"],function(H) {
    var UISelf = null;
    function UI() {
        UISelf = this;
    }
    UI.prototype.initApp = function () {

    };

    /**
     * 获取安全控件密码
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordSetSk = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.pwdSetSk($("#"+id).attr("rd"), function () {
                        callf('ok');
                });
            } else {
                callf('');
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取安全控件密码
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordResult = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.pwdResult(function (x) {
                    if(x.code == 0){
                        callf(BASE64.encoder(x.data));
                    } else {
                        callf('');
                    }
                });
            } else {
                callf( BASE64.encoder(obj.pwdResult()) );
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取安全控件mac地址
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordMachineNetwork = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.machineNetwork(function (x) {
                    if(x.code == 0){
                        callf(BASE64.encoder(x.data));
                    } else {
                        callf('');
                    }
                });
            } else {
                callf( BASE64.encoder(obj.machineNetwork()) );
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取安全控件CPU ID
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordMachineCPU = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.machineCPU(function (x) {
                    if(x.code == 0){
                        callf(BASE64.encoder(x.data));
                    } else {
                        callf('');
                    }
                });
            } else {
                callf( BASE64.encoder(obj.machineCPU()) );
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取安全控件磁盘ID
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordMachineDisk = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.machineDisk(function (x) {
                    if(x.code == 0){
                        callf(BASE64.encoder(x.data));
                    } else {
                        callf('');
                    }
                });
            } else {
                callf( BASE64.encoder(obj.machineDisk()) );
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取密码长度
     * @param id
     * @returns {*}
     */
    UI.prototype.getPasswordLength = function (id, callf) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null && callf != null){
            if(UISelf.getCheckOsBrowser(id) == 10 || UISelf.getCheckOsBrowser(id) ==11){
                obj.pwdLength(function (x) {
                    if(x.code == 0){
                        callf(x.data);
                    } else {
                        callf('');
                    }
                });
            } else {
                callf(obj.pwdLength());
            }
        } else {
            if(!!callf) callf('');
        }
    };

    /**
     * 获取浏览器的类型
     * @param id
     * @returns {*}
     */
    UI.prototype.getCheckOsBrowser = function (id) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null){
            return obj.osBrowser;
        } else {
            return '';
        }
    };

    /**
     * 获取密码控件是否安装
     * @param id
     * @returns {*}
     */
    UI.prototype.getCheckIsInstalled = function (id) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null){
            return obj.isInstalled;
        } else {
            return '';
        }
    };

    /**
     * 获取密码控件的版本
     * @param id
     * @returns {*}
     */
    UI.prototype.getPgeVersion = function (id) {
        var obj = window['SECURITY_PASSWORD_'+id];
        if(obj != null){
            return obj.pgeVersion;
        } else {
            return '';
        }
    };

    /**
     * 输入框控件
     * @param settings JSON
     * settings : {"ID":"String","Type":['password','phone','bankCard','number'],"Decimal":"Int"}
     * @constructor
     */
    UI.prototype.InputObject = function(settings){
        var InputObjectSelf = null;
        //解决IE8 placeholder 不兼容的问题
        UISelf.setPlaceholder();

        function InputObject(){
            InputObjectSelf = this;
            if(settings["ID"] == null || settings["ID"] == '' ){
                console.error("InputObject settings ID is null");
                return;
            }
            this.ID = settings["ID"];
            if(settings["Type"] == null || settings["Type"] == ''){
                console.error("InputObject settings ID is null");
                return;
            }
            this.Type = settings["Type"];
            if(settings["Decimal"] == null || settings["Decimal"] == ''){
                this.decimal = null;
            } else {
                this.decimal = settings["Decimal"];
            }

            this.inputVal = null;
            this.inputObj = document.getElementById(this.ID);
            this.init();
        }

        InputObject.prototype.init = function(){
            var self = this;
            switch(self.Type){
                case 'password':
                    self.InitPassword();
                    break;
                case 'phone':
                case 'number':
                case 'bankCard':
                    self.InitDefaultInput();
                    break;
                default :

                break;
            }
            self.setFormat();
        };
        /**
         * 密码控件
         * @constructor
         */
        InputObject.prototype.InitPassword = function () {
            var pass_id = InputObjectSelf.ID.replace("-show","");

            $("#"+InputObjectSelf.ID).click(function(){
                if(UISelf.getCheckIsInstalled(pass_id) == false || UISelf.getPgeVersion(pass_id) == ''){
                    $("#"+InputObjectSelf.ID).val("").attr("my-placeholder","");
                    $(InputObjectSelf.inputObj.parentNode).find(".input-pass").css("z-index","0");
                    if(UISelf.getCheckOsBrowser(pass_id) == 1 || UISelf.getCheckOsBrowser(pass_id) == 3){
                        $("#"+pass_id+"-self").height("22");
                    }
                    return;
                }

                $("#"+InputObjectSelf.ID).val("");
                $(InputObjectSelf.inputObj.parentNode).find(".input-pass").css("z-index","0");
                if(UISelf.getCheckOsBrowser(pass_id) == 1 || UISelf.getCheckOsBrowser(pass_id) == 3){
                    $("#"+pass_id+"-self").height("22");
                }
                $("#"+pass_id+"-self").focus();
                setTimeout(function(){
                    $("#"+pass_id+"-self").focus();
                },500);
                config.passOnBlur = function(){

                    UISelf.getPasswordLength(pass_id,function (length) {
                        if(length == 0){
                            $(InputObjectSelf.inputObj.parentNode).find(".input-pass").css("z-index","-1");
                            if(UISelf.getCheckOsBrowser(pass_id) == 1 || UISelf.getCheckOsBrowser(pass_id) == 3){
                                $("#"+pass_id+"-self").height("0");
                            }
                        }
                    });

                };

            });

        };

        /**
         * 通用输入框
         * @constructor
         */
        InputObject.prototype.InitDefaultInput = function () {
            $(this.inputObj).keyup(function() {
                InputObjectSelf.setInputType(InputObjectSelf.inputObj.value, this);
            });
        };

        /**
         * Restricted content
         * @param val : input value
         * @param valObj : input DOM object
         */
        InputObject.prototype.setInputType = function(val,valObj){
            var self = this;
            if (self.Type == null || self.Type == 'undefined') {
                return;
            }
            switch(self.Type){
                case 'number':
                    if(self.decimal == null || self.decimal == undefined){
                        $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                    } else {
                        var thisVal = $(valObj).val();
                        if(thisVal.indexOf('.') == 0 || (thisVal.indexOf('.') >0 && (thisVal.split('.').length > 2 || thisVal.split('.')[1].length > self.decimal*1)) ) {
                            if(thisVal.split('.').length > 2){
                                $(valObj).val($(valObj).attr("oldVal"));
                            } else {
                                $(valObj).val(thisVal.substring(0,thisVal.length-1));
                            }
                        }
                        if(thisVal.indexOf('0') == 0 && thisVal.length > 1 && thisVal.indexOf('.') != 1) {
                            $(valObj).val(thisVal.substring(0,thisVal.length-1));
                        }
                        var newVal = $(valObj).val().replace(/[^0-9.]+/,'');
                        $(valObj).val(newVal);
                        $(valObj).attr("oldVal",newVal);
                    }
                    break;
                case 'bankCard':
                case 'phone':
                    $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                    break;
                case 'IDCard':
                    $(valObj).val($(valObj).val().replace(/[^0-9Xx]+/,""));
                    break;
                default :
                    $(valObj).val($(valObj).val().replace(/[^0-9]+/,''));
                    break;
            }
        };

        /**
         * According to the type of formatted content
         */
        InputObject.prototype.setFormat = function(){
            var self = this;
            if (self.Type == null || self.Type == 'undefined') {
                return;
            }
            var inputObj = $(self.inputObj);
            switch(self.Type){
                case 'number':

                    break;
                case 'phone':
                    $(self.inputObj).keyup(function() {
                        inputObj.val(inputObj.val().replace(/\s/g,'').replace(/(\d{3})(\d{4})(?=\d)/g,"$1 $2 "));
                    });
                    break;
                case 'bankCard':
                    $(self.inputObj).keyup(function() {
                        inputObj.val(inputObj.val().replace(/\s/g,'').replace(/(\d{4})(?=\d)/g,"$1 "));
                    });
                    break;
            }
        };

        InputObject.prototype.getToEmptyValue = function() {
            if(this.Type == 'password'){
                var pass_id = InputObjectSelf.ID.replace("-show","");
                return  UISelf.getPasswordResult(pass_id);
            }
            return this.inputObj.value.replace(/\s/g,'');
        };

        return new InputObject();
    };

    /**
     * 文件下载
     * @param settings
     * settings : {"ID":"String","Url":"String","data":"JSON","formAttributes":"JSON","Success":"function"}
     * @constructor
     */
    UI.prototype.ExportFile = function (settings) {
        var ExportFileSelf = null;
        var H = Highcharts;
        function ExportFile() {
            ExportFileSelf = this;
            this.init();
        }

        ExportFile.prototype.init = function () {
            this.data = null;
            this.formAttributes = null;
            if(settings["data"] != null){
                this.data = settings["data"];
            }
            if(settings["formAttributes"] != null){
                this.formAttributes = settings["formAttributes"];
            }
            this.id = settings["ID"];
            this.url = settings["Url"];
            this.Success = settings["Success"];
            this.Create();
        };

        // Create Dom Object
        ExportFile.prototype.Create = function () {
            var name, self = this;

            // create the form
            self.form = H.createElement('form', H.merge({
                method: 'post',
                action: self.url,
                target: "hidden_frame",
                enctype: 'multipart/form-data'
            }, self.formAttributes), {
                display: 'none'
            }, H.doc.getElementById(self.id));

            // add the data
            for (name in self.data) {
                H.createElement('input', {type: 'hidden', name: name, value: self.data[name]},
                    null, self.form);
            }

            // add submit Btn
            self.submitBtn = H.createElement('input',H.merge({type:"submit", name:"hidden_frame"}),
                {display: 'none'}, self.form);

            // add file input
            self.iframe = H.createElement('iframe',H.merge({id:"hidden_frame", name:"hidden_frame"}),
                {display: 'none'}, self.form);
        };


        // Triggering Submit btn
        ExportFile.prototype.Submit = function () {
            // submit
            this.form.submit();

            H.discardElement(this.selectbtn);
            // add file input
            this.selectbtn = H.createElement('input',H.merge({type:"file", name:"file"}),
                {display: 'none'}, this.form);

            // listener callback
            //this.WatcherSuccess();
        };

        // listener callback
        ExportFile.prototype.WatcherSuccess = function () {
            var timeAccumula = 0;
            var interval = setInterval(function () {
                var jsonStr = $(ExportFileSelf.iframe.contentWindow.document.body).text();
                timeAccumula++;
                if(timeAccumula >= 7500){
                    clearInterval(interval);
                    ExportFileSelf.Success({"code":"999996","content":"超时"});
                }
                if(jsonStr.replace(/\s/g,'').length != 0) {
                    clearInterval(interval);
                    console.info(jsonStr);
                    var json = JSON.parse(jsonStr);
                    ExportFileSelf.Success(json);
                }
            },1);
        };
        return new ExportFile();
    };



    /**
     * Loading框
     * @param settings JSON
     * FuncBoxBG : 背景click 事件
     * @constructor
     */
    UI.prototype.LoadingDialog = function(settings){
        function LoadingDialog(){
            this.Box_bg = document.createElement("div");
            this.Box_bg.className = "LoadingDialogBox_bg";
            this.Box_bg.onclick = function(){
                if(settings != null && "function" == typeof settings.FuncBoxBG){
                    settings.FuncBoxBG();
                }
            };
            this.Box = document.createElement("div");
            this.Box.className = "LoadingDialogBox";
            this.init();
        }

        LoadingDialog.prototype.init = function(){
            document.documentElement.appendChild(this.Box_bg);
            document.documentElement.appendChild(this.Box);
        };

        LoadingDialog.prototype.showLoading = function () {
            $(this.Box_bg).show();
            $(this.Box).show();
        };

        LoadingDialog.prototype.hideLoading = function () {
            $(this.Box_bg).hide();
            $(this.Box).hide();
        };

        LoadingDialog.prototype.removeLoading = function () {
            document.documentElement.removeChild(this.Box_bg);
            document.documentElement.removeChild(this.Box);
            config.loadingDialog = null;
        };
        return new LoadingDialog();
    };

    //解决IE8 placeholder 不兼容的问题
    UI.prototype.setPlaceholder = function() {
        jQuery('[my-placeholder]').focus(function() {
            var input = jQuery(this);
            if (input.val() == input.attr('my-placeholder')) {
                input.val('');
                input.removeClass('placeholder');
            }
            input.parent().parent().addClass("input-wrap-focus");
        }).blur(function() {
            var input = jQuery(this);
            if (input.val() == '' || input.val() == input.attr('my-placeholder')) {
                input.addClass('placeholder');
                input.val(input.attr('my-placeholder'));
                if(jQuery(this).parent().find('.winA').text() == '请点此安装控件'
                    || jQuery(this).parent().find('.ocx_style').text() == '请点此安装控件'){
                    input.click();
                }
            }
            input.parent().parent().removeClass("input-wrap-focus");
        }).blur().parents('form').submit(function() {
            jQuery(this).find('[my-placeholder]').each(function() {
                var input = jQuery(this);
                if (input.val() == input.attr('my-placeholder')) {
                    input.val('');
                }
            })
        });
    };
    return new UI();
});