package cn.com.bestpay.portal.config;

import cn.com.bestpay.portal.config.filter.tool.GetPermissonList;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Created by Howell on 16/12/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
//@ContextConfiguration(classes = {Initializer.class})
@ContextConfiguration({"classpath:test-context.xml"})
@ActiveProfiles("mock")
public abstract class SetupConfig<T> {

    public T request;

    @Before
    public void setUp() {
        GetPermissonList getPermissonList = new GetPermissonList();
        MockitoAnnotations.initMocks(this);
        init();
    }




    public abstract void init();

}
