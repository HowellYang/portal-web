package cn.com.bestpay.portal.controller;

import cn.com.bestpay.portal.config.SetupConfig;
import cn.com.bestpay.portal.controller.api.LoginController;
import cn.com.bestpay.portal.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * Created by Howell on 15/12/16.
 */
@Slf4j
public class LoginControllerTest extends SetupConfig {
    private MockMvc mockMvc;

    private MockHttpServletRequest request;

    private MockHttpServletResponse response;


    @InjectMocks
    @Autowired
    LoginController loginController;

    @Spy
    @Autowired
    LoginService loginService;


    @Override
    public void init() {
        request = new MockHttpServletRequest();
        request.setCharacterEncoding("UTF-8");
        response = new MockHttpServletResponse();

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(loginController,loginService).build();
    }

    /**
     * 直接通用Service 方法
     * @throws Exception
     */
    @Test
    public void loginService() throws Exception{
        try {
            request.setParameter("username", "15270878808");
            request.setParameter("loginpwd", "123456");

            assertEquals(true, loginService.userLogin(request).get());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * mockMvc 模拟请求Controller
     * @throws Exception
     */
    @Test
    public void loginController() throws Exception {
        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<String, String>();
        multiValueMap.add("username", "15270878808");
        multiValueMap.add("loginpwd", "123456");
        multiValueMap.add("machineNetwork", "");
        multiValueMap.add("machineCPU", "");
        multiValueMap.add("machineDisk", "");

        mockMvc.perform(
                post("/api/login")
                        .contentType(MediaType.ALL)
                        .params(multiValueMap)
        ).andExpect(status().isOk()).andDo(print()).andReturn(); //执行请求

    }


    /**
     * 直接通用Controller 方法
     * @throws Exception
     */
    @Test
    public void loginControllerEquals() throws Exception {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("username","15270878808");
        jsonObject.put("loginpwd","123456");
        request.setParameter("username", "123456");
        request.setParameter("loginpwd", "123456");

        assertEquals("redirect:/Index/main.html#/Index", loginController.userLogin(request,request.getSession()).getViewName());
    }


}
