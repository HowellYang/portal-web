package cn.com.bestpay.portal.controller;

import cn.com.bestpay.portal.config.SetupConfig;
import cn.com.bestpay.portal.controller.api.LogoutController;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Howell on 21/12/16.
 */
public class LogoutControllerTest extends SetupConfig {
    private MockMvc mockMvc;

    private MockHttpServletRequest request;

    @InjectMocks
    @Autowired
    LogoutController logoutController;

    @Override
    public void init() {
        request = new MockHttpServletRequest();

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(logoutController).build();
    }

    @Test
    public void logoutController() throws Exception {
        mockMvc.perform(
                get("/api/logout")
                        .contentType(MediaType.ALL)
        ).andExpect(status().isOk()).andDo(print()).andReturn(); //执行请求
    }

    @Test
    public void logoutControllerEquals() throws Exception {

        assertEquals("redirect:/Index/main.html", logoutController.userLogout(request.getSession()).getViewName());
    }
}
