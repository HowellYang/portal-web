package cn.com.bestpay.portal.controller.api.index;

import cn.com.bestpay.portal.config.SetupConfig;
import cn.com.bestpay.portal.model.pojo.UtilsModel.UserInfoModel;
import cn.com.bestpay.portal.service.api.index.IndexService;
import lombok.extern.slf4j.Slf4j;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpSession;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Howell on 19/12/16.
 */
@Slf4j
public class IndexControllerTest extends SetupConfig {
    private MockMvc mockMvc;


    @InjectMocks
    @Autowired
    IndexController indexController;

    @Spy
    @Autowired
    IndexService indexService;

    @Autowired
    HttpSession session;

    @Override
    public void init() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(indexController,indexService,session).build();
    }

    @Test
    public void testRestMain() {
        UserInfoModel userInfoModel = new UserInfoModel();
        userInfoModel.setMachineCPU("");
        userInfoModel.setMachineDisk("");
        userInfoModel.setMachineNetwork("");

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("isCheckDeviceInfo","Y");
        jsonObject.put("MachineNetwork","");
        jsonObject.put("MachineDisk","");
        jsonObject.put("MachineCPU","");

        try {
            mockMvc.perform(
                    MockMvcRequestBuilders
                            .post("/api/index/main")
                            .sessionAttr("userSession",userInfoModel)
                            .contentType(MediaType.APPLICATION_JSON_UTF8)
                            .content(jsonObject.toString())
            ).andExpect(status().isOk()).
                    andDo(print()).
                    andReturn(); //执行请求
        } catch (Exception e) {
            log.error(e.getMessage().toString());
        }
    }


}
