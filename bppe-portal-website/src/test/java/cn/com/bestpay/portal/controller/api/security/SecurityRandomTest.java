package cn.com.bestpay.portal.controller.api.security;

import cn.com.bestpay.portal.SecurityScript.SecurityHTML;
import cn.com.bestpay.portal.config.SetupConfig;
import cn.com.bestpay.portal.model.pojo.UtilsModel.UserInfoModel;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.servlet.http.HttpSession;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by Howell on 16/12/16.
 */
@Slf4j
public class SecurityRandomTest extends SetupConfig {
    private MockMvc mockMvc;

    @InjectMocks
    @Autowired
    SecurityRandomController securityRandomController;

    @Spy
    @Autowired
    SecurityHTML securityHTML;

    @Autowired
    HttpSession session;

    @Override
    public void init() {

        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(securityRandomController,securityHTML,session).build();
    }


    @Test
    public void testSpeedGetRandom() throws Exception {
        UserInfoModel userInfoModel = new UserInfoModel();

        mockMvc.perform(
                MockMvcRequestBuilders
                        .post("/api/security/random")
                        .sessionAttr("userSession",userInfoModel)
                        .contentType(MediaType.APPLICATION_JSON_UTF8)
        ).andExpect(status().isOk()).
                andDo(print()).
                andReturn(); //执行请求

    }


}
